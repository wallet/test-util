package com.budgetbakers.mock.braintree

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

@JacksonXmlRootElement(localName = "subject")
data class Subject(
  val subscription: Subscription? = null
)
