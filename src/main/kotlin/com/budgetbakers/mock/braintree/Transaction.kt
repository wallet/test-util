package com.budgetbakers.mock.braintree

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

@JacksonXmlRootElement(localName = "transaction")
data class Transaction(
  @JacksonXmlProperty(localName = "id") val id: String? = null,
  @JacksonXmlProperty(localName = "amount") val amount: String? = null,
  @JacksonXmlProperty(localName = "customer") val customer: Customer? = null
)
