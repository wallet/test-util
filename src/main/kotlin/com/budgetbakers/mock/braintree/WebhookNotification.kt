package com.budgetbakers.mock.braintree

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

@JacksonXmlRootElement(localName = "xml")
data class WebhookNotification(val kind: String, val subject: Subject)
