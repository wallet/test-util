package com.budgetbakers.mock.braintree

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

@JacksonXmlRootElement(localName = "customer")
data class Customer(
  @JacksonXmlProperty(localName = "id") val id: String
)
