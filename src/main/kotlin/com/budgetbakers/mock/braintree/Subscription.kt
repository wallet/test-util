package com.budgetbakers.mock.braintree

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

@JacksonXmlRootElement(localName = "subscription")
data class Subscription(
  @JacksonXmlProperty(localName = "id") val id: String? = null,
  @JacksonXmlProperty(localName = "plan-id") val planId: String? = null,
  @JacksonXmlProperty(localName = "next-billing-period-amount") val nextBillingPeriodAmount: String? = null,
  @JacksonXmlProperty(localName = "next-billing-date") val nextBillingDate: String? = null,
  @JacksonXmlProperty(localName = "merchant-account-id") val merchantAccountId: String? = null,
  @JacksonXmlProperty(localName = "payment-method-token") val paymentMethodToken: String? = null,
  @JacksonXmlProperty(localName = "failure-count") val failureCount: Int? = null,
  @JacksonXmlProperty(localName = "transaction") @JacksonXmlElementWrapper(localName = "transactions") val transactions: List<Transaction>? = null
)
