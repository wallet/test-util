@file:Suppress("SameParameterValue")

package com.budgetbakers.queue

import com.budgetbakers.be.api.model.notify.UserNotifyRequest
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class QueueSender {

    @Autowired
    private lateinit var template: RabbitTemplate

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    fun userConfigure(userNotifyRequest: UserNotifyRequest) {
        send("user-configure-local", userNotifyRequest)
    }

    fun billingWorker(body: Map<String, Any>) {
        send("billing-worker-local", body)
    }

    private fun send(queueName: String, payload: Any) {
        template.convertAndSend(queueName, objectMapper.writeValueAsString(payload))
    }
}
