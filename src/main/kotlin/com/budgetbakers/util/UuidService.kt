package com.budgetbakers.util

import org.springframework.stereotype.Service
import java.util.*

@Service
class UuidService {

    fun uuid(): String {
        return UUID.randomUUID().toString()
    }
}
