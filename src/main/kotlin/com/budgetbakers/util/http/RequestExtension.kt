package com.budgetbakers.util.http

import com.budgetbakers.couchdb.model.CouchCredentials
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.http.HttpEntity
import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.fluent.Request
import org.apache.http.client.fluent.Response
import org.apache.http.impl.auth.BasicScheme
import org.apache.http.util.EntityUtils
import org.slf4j.LoggerFactory


private val logger = LoggerFactory.getLogger("com.budgetbakers.util.http.RequestExtension")

val objectMapper = ObjectMapper()

fun HttpResponse.validate(error: String): HttpResponse {
    if (this.statusLine.statusCode in 200..205) return this
    logger.warn("HTTP error response: ${EntityUtils.toString(this.entity)}")
    throw RuntimeException("$error: HTTP request failed: ${this.statusLine}")
}

fun HttpResponse.returnValidEntity(error: String): HttpEntity {
    return this.validate(error).entity
}

fun HttpResponse.returnValidString(error: String): String {
    val entity = this.returnValidEntity(error)
    return EntityUtils.toString(entity)
}

inline fun <reified T> HttpResponse.asJson(): T {
    val bodyStr = EntityUtils.toString(this.returnValidEntity("Cannot parse JSON from invalid response"))
    return objectMapper.readValue(bodyStr)
}

fun Response.returnValidEntity(error: String): HttpEntity {
    return this.returnResponse().validate(error).entity
}

fun Request.basicAuth(token: String): Request {
    this.addHeader(AuthUtil.authHeader(token))
    return this
}

fun Request.basicAuth(credentials: CouchCredentials): Request {
    return basicAuth(credentials.token)
}

fun Request.returnValidEntity(error: String): HttpEntity {
    return this.execute().returnValidEntity(error)
}

fun HttpRequest.basicAuth(credentials: CouchCredentials): HttpRequest {
    val creds = UsernamePasswordCredentials(credentials.username, credentials.password)
    this.addHeader(BasicScheme().authenticate(creds, this, null))
    return this
}
