package com.budgetbakers.util.http

import org.apache.http.Header
import org.apache.http.message.BasicHeader
import java.util.*

object AuthUtil {
    fun authHeader(token: String): Header {
        return BasicHeader("Authorization", "Basic " + String(Base64.getEncoder().encode(token.toByteArray())))
    }
}
