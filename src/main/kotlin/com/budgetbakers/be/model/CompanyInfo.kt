package com.budgetbakers.be.model


data class CompanyInfo(val name: String, val industry: Industry, val role: Role) {
  enum class Industry(val code: String) {
    InformationServicesIt("information_services_it");
  }

  enum class Role(val code: String) {
    OwnerCeo("owner_ceo")
  }
}
