package com.budgetbakers.be.model

object Defaults {
    val androidCouchDb = BeRequestMeta(Platform.Android, "5123", null)
    val androidCouchbase = BeRequestMeta(Platform.Android, "7100", null)
    val web = BeRequestMeta(Platform.Web, "1.2.3", Flavor.Wallet)
}
