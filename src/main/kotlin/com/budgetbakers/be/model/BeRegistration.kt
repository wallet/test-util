package com.budgetbakers.be.model

import Flavor

data class BeRegistration(
  val flavor: String = Flavor.Wallet,
  val firstName: String = "",
  val lastName: String = "",
  val email: String,
  val password: String,
  val companyInfo: CompanyInfo? = null
  )
