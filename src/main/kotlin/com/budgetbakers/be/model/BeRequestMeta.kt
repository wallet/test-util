package com.budgetbakers.be.model

data class BeRequestMeta(val platform: String? = null, val version: String? = null, val flavor: String? = null)



