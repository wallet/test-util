package com.budgetbakers.be.mobile

import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.util.http.asJson
import com.budgetbakers.util.http.validate
import com.ribeez.RibeezProtos
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ByteArrayEntity
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service


@Service
class WakeUpAcknowledgeClient(val httpClient: HttpClient) {

  @Value("\${budgetbakers.api.url}")
  lateinit var backendUrl: String

  @Value("\${budgetbakers.api.token}")
  lateinit var staticToken: String

  private val ribeezEndpoint = "/ribeez"

  fun acknowledge(context: TestContext) {
    val userResponse = httpClient.execute(HttpPost("$backendUrl$ribeezEndpoint/wakeup-acknowledge"), context.httpContext)
    userResponse.validate("acknowledge response is not success")
  }

  fun install(context: TestContext, installation: RibeezProtos.Installation) {
    val request = HttpPost("$backendUrl$ribeezEndpoint/installation")
    request.entity = ByteArrayEntity(installation.toByteArray())
    val installationResponse = httpClient.execute(request, context.httpContext)
    installationResponse.validate("install response is not success")
  }

  fun initJobs(limit: Int? = null) {
    val r = HttpPost("$backendUrl/scheduled/wakeup-init-jobs${if (limit != null) "?limit=$limit" else ""}")
    r.addHeader("X-Auth-Token", staticToken)
    val response = httpClient.execute(r)
    val data: Map<String, List<String>> = response.asJson()
    println("data: ${data["userIds"]}")
  }
}
