package com.budgetbakers.be.billing

import com.budgetbakers.be.Configuration
import com.budgetbakers.be.auth.AuthUtils.addFlavorHeader
import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.util.http.returnValidEntity
import com.budgetbakers.util.http.returnValidString
import com.budgetbakers.util.http.validate
import com.ribeez.RibeezBillingProtos
import com.ribeez.RibeezBillingProtos.BraintreePaymentInfo
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpPut
import org.apache.http.entity.ByteArrayEntity
import org.springframework.stereotype.Service

@Service
class BraintreeClient(private val httpClient: HttpClient, private val cfg: Configuration) {

  private val billingEndpoint = "/ribeez/billing"
  private val urlBraintreeToken: String get() = "${cfg.backendUrl}$billingEndpoint/braintree/v2/token"
  private val urlBraintreeTransaction: String get() = "${cfg.backendUrl}$billingEndpoint/braintree/v2/transaction"
  private val urlSubscription: String get() = "${cfg.backendUrl}$billingEndpoint/braintree/v2/subscription"
  private val urlLifetime: String get() = "${cfg.backendUrl}$billingEndpoint/braintree/v2/lifetime"
  private val urlBraintreePaymentMethod: String get() = "${cfg.backendUrl}$billingEndpoint/braintree/v2/payment-method"
  private val urlBraintreeAddress: String get() = "${cfg.backendUrl}$billingEndpoint/braintree/v2/billing-address"
  private val urlBraintreeDetails: String get() = "${cfg.backendUrl}$billingEndpoint/braintree/v2/details"
  private val urlBraintreeProducts: String get() = "${cfg.backendUrl}$billingEndpoint/braintree/v2/products"

  fun getToken(context: TestContext): String {
    return httpClient.execute(HttpGet(urlBraintreeToken), context.httpContext)
      .returnValidString("Can't get Braintree token")
  }

  fun getProducts(context: TestContext): BraintreePaymentInfo {
    val request = addFlavorHeader(HttpGet(urlBraintreeProducts), context)

    val paymentInfoResponse = httpClient.execute(request, context.httpContext)
      .returnValidEntity("Can't get Braintree products")
    return BraintreePaymentInfo.parseFrom(paymentInfoResponse.content)
  }

  fun getTransaction(context: TestContext): List<RibeezBillingProtos.BraintreeTransaction> {
    val paymentInfoResponse = httpClient.execute(HttpGet(urlBraintreeTransaction), context.httpContext)
      .returnValidEntity("Can't get Braintree transactions")
    return RibeezBillingProtos.BraintreeTransactionList.parseFrom(paymentInfoResponse.content).transactionsList
  }

  fun createSubscription(context: TestContext, transaction: RibeezBillingProtos.Transaction) {
    val request = HttpPost(urlSubscription)
    request.entity = ByteArrayEntity(transaction.toByteArray())
    handleCreateTransaction(request, context)
  }

  fun cancelSubscription(context: TestContext): HttpResponse {
    return httpClient.execute(HttpDelete(urlSubscription), context.httpContext).validate("Cancel subscription failed")
  }

  fun getDetails(context: TestContext): RibeezBillingProtos.BraintreeDetails {
    val paymentMethodResponse = httpClient.execute(addFlavorHeader(HttpGet(urlBraintreeDetails), context), context.httpContext).returnValidEntity("Can't get Braintree details")
    return RibeezBillingProtos.BraintreeDetails.parseFrom(paymentMethodResponse.content)
  }

  fun updatePaymentMethod(context: TestContext, nonce: String): HttpResponse {
    val request = HttpPost(urlBraintreePaymentMethod)
    request.entity = ByteArrayEntity(RibeezBillingProtos.BraintreeNonce.newBuilder().setNonce(nonce).build().toByteArray())
    return httpClient.execute(request, context.httpContext).validate("Update Payment method not successful")
  }

  fun updateBillingAddress(context: TestContext, address: RibeezBillingProtos.BraintreeBillingAddress): HttpResponse {
    val request = addFlavorHeader(HttpPut(urlBraintreeAddress), context)
    request.entity = ByteArrayEntity(address.toByteArray())
    return httpClient.execute(request, context.httpContext).validate("Update address not successful")
  }

  fun createLifetime(context: TestContext, transaction: RibeezBillingProtos.Transaction) {
    val request = HttpPost(urlLifetime)
    request.entity = ByteArrayEntity(transaction.toByteArray())
    handleCreateTransaction(request, context)
  }

  private fun handleCreateTransaction(request: HttpPost, context: TestContext) {
    val response = httpClient.execute(addFlavorHeader(request, context), context.httpContext)
    if (response.statusLine.statusCode == 422) {
      val error = RibeezBillingProtos.BraintreeErrorResponse.parseFrom(response.entity.content)
      throw Exception("Error=${error.message} ${error.gatewayRejectionReason} ${error.transactionStatus} ${error.threeDSecureStatus}")
    }
    response.validate("Create transaction failed")
  }
}
