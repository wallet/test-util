package com.budgetbakers.be.billing

import com.budgetbakers.be.Configuration
import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.util.http.returnValidString
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.springframework.stereotype.Service

@Service
class BillingClient(private val httpClient: HttpClient, private val cfg: Configuration) {

  private val billingEndpoint = "/ribeez/billing"
  private val urlBraintreeToken: String get() = "${cfg.backendUrl}$billingEndpoint/braintree/v2/token"

  fun getToken(context: TestContext): String {
    return httpClient.execute(HttpGet(urlBraintreeToken), context.httpContext)
      .returnValidString("Can't get Braintree token")
  }
}
