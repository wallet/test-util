package com.budgetbakers.be.imports

import com.budgetbakers.be.auth.TestContext
import com.ribeez.RibeezProtos
import org.apache.http.HttpStatus
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class ImportsClient(val httpClient: HttpClient) {

  @Value("\${budgetbakers.api.url}")
  lateinit var backendUrl: String

  private fun importsEndpoint() = "$backendUrl/ribeez/import/v1"

  fun sendInfoEmail(context: TestContext) {
    val referralResponse = httpClient.execute(HttpPost("${importsEndpoint()}/sendInfoEmail"), context.httpContext)
    assert(referralResponse.statusLine.statusCode == HttpStatus.SC_OK)
  }

  fun getUserImports(context: TestContext): RibeezProtos.GetUserImportsResponse {
    val referralResponse = httpClient.execute(HttpGet("${importsEndpoint()}/all"), context.httpContext)
    assert(referralResponse.statusLine.statusCode == HttpStatus.SC_OK)
    return RibeezProtos.GetUserImportsResponse.parseFrom(referralResponse.entity.content)
  }
}
