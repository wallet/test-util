package com.budgetbakers.be.api.model.notify

import com.fasterxml.jackson.annotation.JsonValue

enum class ChangeSource(val code: String) {
  General("GENERAL"),
  Sharing("SHARING"),
  Billing("BILLING"),
  ProfileInfo("PROFILE_INFO");

  @JsonValue
  fun toJson(): String {
      return code
  }
}
