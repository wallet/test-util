package com.budgetbakers.be.api.model.notify

import com.fasterxml.jackson.annotation.JsonValue

enum class ChangeType(val code: String) {
  Add("ADD"),
  Modify("MODIFY"),
  Remove("REMOVE");

  @JsonValue
  fun toJson(): String {
    return code
  }
}
