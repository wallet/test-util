package com.budgetbakers.be.api.model.notify


data class UserNotifyRequest(
        val userId: String,
        val changeSource: ChangeSource,
        val changeType: ChangeType,
        val notifyOldDb: Boolean
)
