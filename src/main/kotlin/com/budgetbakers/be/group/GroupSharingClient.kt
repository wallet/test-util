package com.budgetbakers.be.group

import com.budgetbakers.be.Configuration
import com.budgetbakers.be.auth.AuthUtils
import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.util.http.returnValidEntity
import com.budgetbakers.util.http.validate
import com.ribeez.RibeezProtos
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ByteArrayEntity
import org.springframework.stereotype.Component

@Component
class GroupSharingClient(private val httpClient: HttpClient, private val cfg: Configuration) {

  private val groupEndpoint: String get() = "${cfg.backendUrl}/ribeez/group"

  fun getGroup(context: TestContext): RibeezProtos.Group {
    val getRequest = HttpGet(groupEndpoint)
    getRequest.addHeader(AuthUtils.getFlavorHeader(context))
    val userResponse = httpClient.execute(getRequest, context.httpContext).returnValidEntity("Can't get group")
    return RibeezProtos.Group.parseFrom(userResponse.content)
  }

  fun submitGroup(context: TestContext, group: RibeezProtos.CreateGroupRequest): HttpResponse {
    val request = HttpPost(groupEndpoint)
    request.addHeader(AuthUtils.getFlavorHeader(context))
    request.entity = ByteArrayEntity(group.toByteArray())
    return httpClient.execute(request, context.httpContext).validate("Cannot create group ${group.name}")
  }

  fun deleteGroup(context: TestContext): RibeezProtos.Group {
    val request = HttpDelete(groupEndpoint)
    request.addHeader(AuthUtils.getFlavorHeader(context))
    val response = httpClient.execute(request, context.httpContext).returnValidEntity("Can't delete group")
    return RibeezProtos.Group.parseFrom(response.content)
  }

  fun getGroupUser(context: TestContext, email: String): RibeezProtos.GroupUser {
    val request = HttpGet("$groupEndpoint/member-detail/$email")
    request.addHeader(AuthUtils.getFlavorHeader(context))
    val response = httpClient.execute(request, context.httpContext).returnValidEntity("Can't get group user by email=$email")
    return RibeezProtos.GroupUser.parseFrom(response.content)
  }

  fun leaveGroup(context: TestContext, ownerId: String) {
    val request = HttpDelete("$groupEndpoint/leave/$ownerId")
    request.addHeader(AuthUtils.getFlavorHeader(context))
    httpClient.execute(request, context.httpContext).validate("Can't leave ownerId=$ownerId group")
  }
}
