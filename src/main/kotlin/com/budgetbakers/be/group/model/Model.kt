package com.budgetbakers.be.group.model

import com.ribeez.RibeezProtos

abstract class GroupModelPermission(modelType: RibeezProtos.ModelType, modulePermission: RibeezProtos.GroupAccessPermission) {

  private val builder = RibeezProtos.ModulePermission.newBuilder()
    .setModulePermissionType(RibeezProtos.ModulePermissionType.MODEL)
    .setModelType(modelType)
    .setModulePermission(modulePermission)

  protected fun protoBuilder(): RibeezProtos.ModulePermission.Builder {
    return builder.clone()
  }

  abstract fun toProto(): RibeezProtos.ModulePermission
}

data class GroupAccountPermission(val objectId: String, val permission: RibeezProtos.GroupAccessPermission) : GroupModelPermission(RibeezProtos.ModelType.Account, RibeezProtos.GroupAccessPermission.READ_WRITE_MODIFY) {
  override fun toProto(): RibeezProtos.ModulePermission {
    val modelType = RibeezProtos.ModelType.Account
    val builder = protoBuilder()
      .setModelType(modelType)
      .addModuleObjectPermissions(
        RibeezProtos.ModuleObjectPermission.newBuilder()
          .setObjectId(objectId)
          .setGroupAccessPermission(permission)
      )
    return builder.build()
  }
}

data class GroupMember(val id: String? = null, val email: String, val modulePermissions: List<GroupModelPermission> = emptyList()) {
  fun toCreateProto(): RibeezProtos.CreateGroupRequestMember {
    val member = RibeezProtos.CreateGroupRequestMember
      .newBuilder()
      .setEmail(email)
    if (id != null) {
      member.id = id
    }
    modulePermissions.forEach { permission -> member.addModulePermission(permission.toProto()) }
    return member.build()
  }

  object Builder {
    fun from(user: RibeezProtos.GroupUser): GroupMember {
      return GroupMember(id = user.userId, email = user.email, modulePermissions = emptyList())
    }

    fun from(member: RibeezProtos.GroupMemberAnonymous): GroupMember {
      return GroupMember(email = member.email)
    }

    fun from(member: RibeezProtos.GroupMember): GroupMember {
      return GroupMember(id = member.user.userId, email = member.user.email)
    }
  }
}

data class Group(val name: String, val groupMembers: List<GroupMember> = emptyList()) {
  fun toCreateProto(): RibeezProtos.CreateGroupRequest {
    val group = RibeezProtos.CreateGroupRequest
      .newBuilder()
      .setName(name)
    groupMembers.forEach { member -> group.addMembers(member.toCreateProto()) }
    return group.build()
  }

  fun addMember(existingGroupUser1: RibeezProtos.GroupUser): Group {
    val groupMembers = ArrayList<GroupMember>()
    groupMembers.addAll(this.groupMembers)
    groupMembers.add(GroupMember.Builder.from(existingGroupUser1))
    return this.copy(groupMembers = groupMembers)
  }

  object Builder {
    fun from(group: RibeezProtos.Group): Group {
      val groupMembers = ArrayList<GroupMember>()
      groupMembers.addAll(group.groupMemberAnonymousList.map { m -> GroupMember.Builder.from(m) })
      groupMembers.addAll(group.groupMemberList.map { m -> GroupMember.Builder.from(m) })
      return Group(name = group.name, groupMembers = groupMembers)
    }
  }
}
