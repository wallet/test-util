package com.budgetbakers.be.dev

import com.budgetbakers.be.Configuration
import com.budgetbakers.mock.braintree.WebhookNotification
import com.budgetbakers.util.http.validate
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DevFeatureClient(private val httpClient: HttpClient, private val cfg: Configuration) {

  private val braintreeMockWebhook = "/ribeez/billing/braintree/mock-webhook"
  @Autowired
  lateinit var xmlMapper: ObjectMapper

  fun sendMockWebhook(webhook: WebhookNotification) {
    val request = HttpPost("${cfg.backendUrl}${braintreeMockWebhook}")
    request.entity = StringEntity(xmlMapper.writeValueAsString(webhook))
    httpClient.execute(request).validate("Could not send mock webhook: $webhook")
  }
}
