package com.budgetbakers.be.auth

import org.apache.http.Header
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.message.BasicHeader

object AuthUtils {
  fun getFlavorHeader(context: TestContext): Header {
    return BasicHeader("flavor", context.flavor)
  }

  fun <T : HttpUriRequest> addFlavorHeader(request: T, context: TestContext): T {
    request.addHeader(getFlavorHeader(context))
    return request
  }
}
