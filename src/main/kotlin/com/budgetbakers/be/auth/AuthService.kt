package com.budgetbakers.be.auth

import com.budgetbakers.be.model.BeRegistration
import com.budgetbakers.be.model.BeRequestMeta
import com.budgetbakers.be.model.Defaults
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AuthService {

    private val log = LoggerFactory.getLogger(this::class.qualifiedName)

    @Autowired
    private lateinit var authClient: AuthClient

    fun register(registration: BeRegistration, meta: BeRequestMeta = Defaults.androidCouchDb) {
        val token = authClient.getRegistrationToken(registration)
        log.info("register(${registration.email}, ${registration.password}) ... got token=$token")
        authClient.registerUser(registration, token, meta)
    }

    fun login(context: TestContext, email: String, password: String) {
       authClient.login(context, email, password)
    }

    fun logout(context: TestContext) {
        authClient.logout(context)
    }
}
