package com.budgetbakers.be.auth

import Platform
import com.budgetbakers.be.model.BeRegistration
import com.budgetbakers.be.model.BeRequestMeta
import com.budgetbakers.util.http.validate
import org.apache.http.HttpStatus
import org.apache.http.client.HttpClient
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class AuthClient(val httpClient: HttpClient) {

  @Value("\${budgetbakers.api.url}")
  lateinit var backendUrl: String

  private val tokenEndpoint = "/auth/signup/token"
  private val signUpEndpoint = "/auth/signup"
  private val loginEndpoint = "/auth/authenticate/userpass"
  private val logoutEndpoint = "/auth/logout"

  fun getRegistrationToken(request: BeRegistration): String {
    val res = httpClient.execute(HttpGet("$backendUrl$tokenEndpoint/${request.email}"))
    assert(res.statusLine.statusCode == HttpStatus.SC_OK)
    return EntityUtils.toString(res?.entity)
  }

  fun registerUser(request: BeRegistration, token: String, requestMeta: BeRequestMeta) {
    val registerForm = listOf(
      BasicNameValuePair("firstName", request.firstName),
      BasicNameValuePair("lastName", request.lastName),
      BasicNameValuePair("password.password1", request.password),
      BasicNameValuePair("password.password2", request.password))
    val registerRequest = HttpPost("$backendUrl$signUpEndpoint/$token")
    // TODO platform, version identification - web has "platform" instead "user-agent" etc.
    // TODO is Android, iOS registered via userpass endpoint?
    if (requestMeta.platform == "android") {
      registerRequest.addHeader("User-Agent", requestMeta.platform)
      registerRequest.addHeader("App-Version-Code", requestMeta.version)
    } else if (requestMeta.platform == Platform.Web || requestMeta.platform == null) {
      registerRequest.addHeader("Platform", requestMeta.platform)
      registerRequest.addHeader("Web-Version-Code", requestMeta.version)
    }
    registerRequest.addHeader("Flavor", requestMeta.flavor)
    registerRequest.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
    registerRequest.entity = UrlEncodedFormEntity(registerForm)
    httpClient.execute(registerRequest).validate("Registration ${request.email} failed")
  }

  fun login(context: TestContext, email: String, password: String) {
    val loginForm = listOf(
      BasicNameValuePair("username", email),
      BasicNameValuePair("password", password))
    val loginRequest = HttpPost("$backendUrl/$loginEndpoint")
    loginRequest.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
    loginRequest.addHeader(AuthUtils.getFlavorHeader(context))
    loginRequest.entity = UrlEncodedFormEntity(loginForm)

    httpClient.execute(loginRequest, context.httpContext).validate("Login failed for $email")
  }

  fun logout(context: TestContext) {
    httpClient.execute(HttpGet("$backendUrl/$logoutEndpoint"), context.httpContext).validate("Logout failed")
  }
}
