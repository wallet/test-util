package com.budgetbakers.be.auth

import org.apache.http.protocol.HttpContext

data class TestContext(val flavor: String, val httpContext: HttpContext)
