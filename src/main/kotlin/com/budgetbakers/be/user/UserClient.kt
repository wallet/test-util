package com.budgetbakers.be.user

import com.budgetbakers.be.Configuration
import com.budgetbakers.be.auth.AuthUtils
import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.util.http.returnValidEntity
import com.budgetbakers.util.http.returnValidString
import com.budgetbakers.util.http.validate
import com.ribeez.RibeezProtos
import com.ribeez.RibeezProtos.Board.CompanyInfo
import com.ribeez.RibeezProtos.User
import org.apache.commons.io.IOUtils
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ByteArrayEntity
import org.apache.http.message.BasicHeader
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.zip.ZipInputStream

@Service
class UserClient(private val httpClient: HttpClient, private val cfg: Configuration) {

  private val userEndpoint = "${cfg.backendUrl}/ribeez/user"

  @Value("\${budgetbakers.api.token}")
  lateinit var staticToken: String

  private val log = LoggerFactory.getLogger(javaClass)

  fun getUser(context: TestContext): User {
    val getRequest = HttpGet(userEndpoint)
    getRequest.addHeader(AuthUtils.getFlavorHeader(context))
    getRequest.addHeader("Platform", "WEB")
    getRequest.addHeader("Web-Version-Code", "4.8.1")
    val userResponse = httpClient.execute(getRequest, context.httpContext)
      .returnValidEntity("Can't get user")
    return User.parseFrom(userResponse.content)
  }

  fun saveUser(user: User, context: TestContext) {
    val request = HttpPost(userEndpoint)
    request.entity = ByteArrayEntity(user.toByteArray())
    httpClient.execute(request, context.httpContext).validate("Cannot save user ${user.userId}")
  }

  fun migrationStart(user: User, context: TestContext) {
    val request = HttpPost("${cfg.backendUrl}${userEndpoint}/couchbase/create")
    httpClient.execute(request, context.httpContext).validate("Cannot start client migration to Couchbase for user ${user.userId}")
  }

  fun getReferralUrl(context: TestContext): String {
    return httpClient.execute(HttpGet("$userEndpoint/referral"), context.httpContext).returnValidString("Can't get referral URL")
  }

  fun updateCompanyInfo(companyInfo: CompanyInfo, context: TestContext) {
    val request = HttpPost("$userEndpoint/company-info")
    request.addHeader(AuthUtils.getFlavorHeader(context))
    request.entity = ByteArrayEntity(companyInfo.toByteArray())
    httpClient.execute(request, context.httpContext).validate("Cannot save company info ${companyInfo.name}")
  }

  fun updateReferralSource(referralSource: RibeezProtos.ReferralSource, context: TestContext) {
    val request = HttpPost("$userEndpoint/referralSource/${referralSource.ordinal}")
    request.addHeader(AuthUtils.getFlavorHeader(context))
    httpClient.execute(request, context.httpContext)
  }

  fun saveVerifiedProfile(context: TestContext, verifiedProfile: RibeezProtos.VerifiedProfile) {
    val request = HttpPost("$userEndpoint/verifiedProfile")
    request.addHeader(AuthUtils.getFlavorHeader(context))
    request.entity = ByteArrayEntity(verifiedProfile.toByteArray())
    httpClient.execute(request, context.httpContext).validate("Could not save verified profile")
  }

  fun exportVerifiedProfile(from: DateTime, to: DateTime): String {
    val datePattern = "yyyy-MM-dd"
    // FIXME BE pro from=to=jeden den nenajde nic, protoze se porovnava date time vs date
    val fromFormatted = from.minusDays(1).toString(datePattern)
    val toFormatted = to.plusDays(1).toString(datePattern)
    val request = HttpGet("${cfg.backendUrl}/api/user/exportProfiles/from/$fromFormatted/to/$toFormatted")
    request.addHeader(BasicHeader("x-auth-token", staticToken))
    val response = httpClient.execute(request).validate("Export verified profiles failed")
    val zipInputStream = ZipInputStream(response.entity.content)

    fun logEntry(): String {
      val zipEntry = zipInputStream.nextEntry
      log.info("exportVerifiedProfile($fromFormatted, $toFormatted) entry name = ${zipEntry.name}")
      return "filename=${zipEntry.name}\n${IOUtils.toString(zipInputStream)}"
    }

    val walletContent = logEntry()
    val boardContent = logEntry()

    assert(zipInputStream.available() == 0) { "Extra ZIP entries." }

    return "${walletContent}\n${boardContent}"
  }

  fun deleteUser(context: TestContext): HttpResponse {
   return httpClient.execute(HttpDelete("${cfg.backendUrl}/ribeez/user"), context.httpContext).validate("Could not delete user")
  }
}
