package com.budgetbakers.be

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class Configuration {

    @Value("\${budgetbakers.api.url}")
    lateinit var backendUrl: String

}
