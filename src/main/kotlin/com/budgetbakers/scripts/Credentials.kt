package com.budgetbakers.scripts

data class Credentials(
        var url: String? = null,
        var dbName: String? = null,
        var username: String? = null,
        var password: String? = null)
