package com.budgetbakers.scripts

import com.budgetbakers.be.model.BeRegistration
import com.budgetbakers.couchdb.model.Document
import com.budgetbakers.couchdb.service.CouchDbService
import com.budgetbakers.scripts.registration.ProfileGenerationService
import com.budgetbakers.scripts.registration.RegistrationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CloneAccountService {

  @Autowired
  private lateinit var couchService: CouchDbService

  @Autowired
  lateinit var profileService: ProfileGenerationService
  @Autowired
  private lateinit var registrationService: RegistrationService

  fun loadFromTemplate(flavor: String, templateName: String): BeRegistration {
    val docs = couchService.loadDumpFromResource(templateName)
    return registrationService.registerWithDocs(profileService.generate(flavor), docs)
  }

  fun loadWithDocs(flavor: String, docs: List<Document>): BeRegistration {
    return registrationService.registerWithDocs(profileService.generate(flavor), docs)
  }
}
