package com.budgetbakers.scripts.application

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class RabbitMqConfig {

    @Bean
    fun userConfigureQueue(): Queue {
        return Queue("user-configure-local")
    }

//    @Bean
//    fun container(connectionFactory: ConnectionFactory): SimpleMessageListenerContainer {
//        val container = SimpleMessageListenerContainer()
//        container.connectionFactory = connectionFactory
//        container.setQueueNames("user-configure-local")
//        val listener = RabbitTemplate(connectionFactory)
//        listener.routingKey = "abcd"
//        listener.setDefaultReceiveQueue("ahoj svete")
//        listener.setReplyAddress("ahoj svete")
//        container.setMessageListener(listener)
//        return container
//    }
}
