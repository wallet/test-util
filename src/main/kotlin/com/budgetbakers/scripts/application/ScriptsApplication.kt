package com.budgetbakers.scripts.application

import com.budgetbakers.be.auth.TestContext
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.apache.http.HttpResponse
import org.apache.http.client.config.CookieSpecs
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.client.BasicCookieStore
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.client.LaxRedirectStrategy
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.apache.http.protocol.BasicHttpContext
import org.apache.http.protocol.HttpContext
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope
import java.util.concurrent.TimeUnit

@SpringBootApplication(scanBasePackages = ["com.budgetbakers"])
class ScriptsApplication {

  @Bean
  fun objectMapper(): ObjectMapper {
    return jacksonObjectMapper()
      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
      .setSerializationInclusion(JsonInclude.Include.NON_NULL)
  }

  @Bean
  fun xmlMapper(): ObjectMapper {
    return XmlMapper(JacksonXmlModule().apply {
      setDefaultUseWrapper(false)
    }).registerKotlinModule()
      .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
      .enable(SerializationFeature.INDENT_OUTPUT)
      .setSerializationInclusion(JsonInclude.Include.NON_NULL)
  }

  @Bean
  fun poolingHttpClientConnectionManager(): PoolingHttpClientConnectionManager {
    val connectionManager = PoolingHttpClientConnectionManager()
    connectionManager.closeIdleConnections(100, TimeUnit.MILLISECONDS)
    // FIMXE connection leak???
    connectionManager.maxTotal = 100
    connectionManager.defaultMaxPerRoute = 100
    return connectionManager
  }

  @Bean
  fun httpClient(connectionManager: PoolingHttpClientConnectionManager): CloseableHttpClient {
    val requestConfig = RequestConfig
      .custom()
      .setConnectionRequestTimeout(5000)
      .setConnectTimeout(30000)
      .setSocketTimeout(1200000)
      .setCookieSpec(CookieSpecs.STANDARD)
      .build()


    return HttpClients
      .custom()
      .setConnectionManager(connectionManager)
      .setRedirectStrategy(LaxRedirectStrategy())
      .setConnectionReuseStrategy { _: HttpResponse, _: HttpContext -> false }
      .setDefaultRequestConfig(requestConfig)
      .build()
  }

  @Bean
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  fun httpContext(flavor: String): TestContext {
    val httpContext = BasicHttpContext()
    httpContext.setAttribute(HttpClientContext.COOKIE_STORE, BasicCookieStore())
    return TestContext(flavor, httpContext)
  }

  companion object {
    @JvmStatic
    fun main(args: Array<String>) {
      runApplication<ScriptsApplication>(*args)
    }
  }
}


