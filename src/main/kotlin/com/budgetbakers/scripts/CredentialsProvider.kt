package com.budgetbakers.scripts


import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "dump-source")
data class CredentialsProvider(
        var account1: Credentials? = null,
        var account2: Credentials? = null,
        var account3: Credentials? = null)

