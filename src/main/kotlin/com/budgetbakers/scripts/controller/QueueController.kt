package com.budgetbakers.scripts.controller

import com.budgetbakers.be.api.model.notify.ChangeSource
import com.budgetbakers.be.api.model.notify.ChangeType
import com.budgetbakers.be.api.model.notify.UserNotifyRequest
import com.budgetbakers.queue.QueueSender
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/queue")
class QueueController {

    @Autowired
    lateinit var queueSender: QueueSender

    @PostMapping(path = ["/user-configure/{userId}/profile-info"])
    fun sendMessage(@PathVariable userId: String): Map<String, Any> {
        val userNotifyRequest = UserNotifyRequest(userId, ChangeSource.ProfileInfo, ChangeType.Modify, false)
        queueSender.userConfigure(userNotifyRequest)
        return mapOf(
                "userId" to userId,
                "userNotifyRequest" to userNotifyRequest
        )
    }

    /*
     def start(userIdOpt: Option[String], countOpt: Option[Int]): Action[AnyContent] = Action.async {
    val count = countOpt.getOrElse(1000)
    log.info(s"Start sending $count messages to userId=$userIdOpt.")
    Future.sequence(Range(0, count).map(id => pushPublisher.send(PushMsg(userId = userIdOpt.getOrElse(s"user-id-$id"), flavor = Flavor.Wallet))))
      .map(_ => Ok(s"$count messages sent."))
  }

  def importNotification(userId: String, flavor: Option[String]): Action[AnyContent] = Action.async {
    Range(0, 200).foreach(_ => {
      pushPublisher.send(PushMsg(
        userId = userId,
        flavor = if (flavor.getOrElse("wallet") == "board") Flavor.Board else Flavor.Wallet,
        notificationType = Some(OPEN_IMPORT),
        param = Some("0"))
      )
    })
    Future.successful(Ok(s"UserId=$userId"))
  }

  def modifyUser(userId: String, value: Option[String]): Action[AnyContent] = Action.async {
    val setValue = Json.toJson(Map("testik" -> value))
    val count = 100
    Future.sequence(Range(0, count).map(_ =>
      modifyUserPublisher.send(ModifyUserRequest(userId, if (value.isDefined) Some(setValue) else None))
    ))
      .map(_ => {
        val msg = s"user $userId modified: $setValue, $count times"
        log.info(msg)
        Ok(msg)
      })
  }
}


     */
    @PostMapping(path = ["/billing-worker"])
    fun billingWorker(@RequestBody body: Map<String, Any>): Map<String, Any> {
        return sendToBillingWorker(body)
    }

    @PostMapping(path = ["/billing-worker/braintree/subscription-charged/{subscriptionId}"])
    fun billingWorkerBraintreeSubsCharged(@PathVariable subscriptionId: String): Map<String, Any> {
        return sendToBillingWorker(mapOf("kind" to "subscription_charged_unsuccessfully", "subscriptionId" to subscriptionId))
    }

    @PostMapping(path = ["/billing-worker/braintree/subscription-canceled/{subscriptionId}"])
    fun billingWorkerBraintreeSubsCanceled(@PathVariable subscriptionId: String): Map<String, Any> {
        return sendToBillingWorker(mapOf("kind" to "subscription_canceled", "subscriptionId" to subscriptionId))
    }

    private fun sendToBillingWorker(body: Map<String, Any>): Map<String, Any> {
        queueSender.billingWorker(body)
        return mapOf("result" to "OK").plus(body)
    }
}
