package com.budgetbakers.scripts.controller

import com.budgetbakers.be.model.BeRegistration
import com.budgetbakers.couchdb.model.Document
import com.budgetbakers.couchdb.service.CouchDbUtil.docsOnly
import com.budgetbakers.scripts.CloneAccountService
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.JsonNodeType
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/clone")
class CloneAccountController {

  private val log = LoggerFactory.getLogger(CloneAccountController::class.java)

  @Autowired
  lateinit var cloneAccountService: CloneAccountService

  @Autowired
  lateinit var objectMapper: ObjectMapper

    @RequestMapping(path = ["/account"], method = [RequestMethod.POST])
    fun cloneFromAccount(): BeRegistration {
      TODO("from production CouchDb account credentials")
    }

    @RequestMapping(path = ["/template"], method = [RequestMethod.POST])
    fun cloneFromTemplate(@RequestParam templateName: String, @RequestParam(defaultValue = Flavor.Wallet) flavor: String): BeRegistration {
        return cloneAccountService.loadFromTemplate(flavor, templateName)
    }

  @RequestMapping(path = ["/file"], method = [RequestMethod.POST], consumes = ["application/json"])
  fun cloneFromFile(@RequestBody request: JsonNode, @RequestParam(defaultValue = Flavor.Wallet) flavor: String): BeRegistration {
    return when (request.nodeType) {
      JsonNodeType.OBJECT -> {
        val map = objectMapper.treeToValue(request, Map::class.java)
        when {
          map.containsKey("rows") -> {
            val docs: List<Document> = convert(map["rows"]).filter(::docsOnly)
            log.info("Cloning account with initial ${docs.size} documents")
            cloneAccountService.loadWithDocs(flavor, docs)
          }
          map.containsKey("results") -> {
            val docs: List<Document> = convert(map["results"]).filter(::docsOnly)
            log.info("Cloning account with initial ${docs.size} documents")
            cloneAccountService.loadWithDocs(flavor, docs)
          }
          else -> {
            throw RuntimeException("Unsupported request, 'rows' or 'results' expected in keys, request keys=${map.keys}.")
          }
        }
      }
      JsonNodeType.ARRAY -> {
        log.info("Cloning account with initial ${request.size()} documents")
        @Suppress("UNCHECKED_CAST")
        cloneAccountService.loadWithDocs(flavor, convert(request))
      }
      else -> {
        throw RuntimeException("Unsupported JSON type=${request.nodeType.name}")
      }
    }
  }

  private fun convert(any: Any?): List<Document> {
    return objectMapper.convertValue(any, objectMapper.typeFactory.constructCollectionType(List::class.java, Document::class.java))
  }
}
