package com.budgetbakers.scripts.registration

import Flavor
import com.budgetbakers.be.model.BeRegistration
import com.budgetbakers.be.model.CompanyInfo
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import java.util.*

@Service
class ProfileGenerationService {

    @Value("\${registration.hashToken}")
    private lateinit var hashToken: String

    @Value("\${registration.password}")
    private lateinit var password: String

    @Value("\${registration.emailTemplate}")
    private lateinit var emailTemplate: String

    @Value("\${registration.firstNameTemplate}")
    private lateinit var firstNameTemplate: String

    @Value("\${registration.lastNameTemplate}")
    private lateinit var lastNameTemplate: String

    @Value("\${registration.companyNameTemplate}")
    private lateinit var companyNameTemplate: String

  fun generate(flavor: String = Flavor.Wallet): BeRegistration {
        val hash = generateHash()
        return BeRegistration(
                flavor = flavor,
                firstName = firstNameTemplate.replaceHash(hash),
                lastName = lastNameTemplate.replaceHash(hash),
                email = emailTemplate.replaceHash(hash),
                password = password.replaceHash(hash),
                companyInfo = if (flavor == Flavor.Board) generateCompanyInfo(hash) else null
        )
    }

    fun generateCompanyInfo(hash: String): CompanyInfo {
        return CompanyInfo(companyNameTemplate.replaceHash(hash), CompanyInfo.Industry.InformationServicesIt, CompanyInfo.Role.OwnerCeo)
    }

    private fun generateHash() = UUID.randomUUID().toString().substring(0, 8)

    private fun String.replaceHash(hash: String): String {
      var value = this
      while (value.contains(hashToken)) {
        value = replace(hashToken, hash)
      }
      return value
  }
}
