package com.budgetbakers.scripts.registration

import com.budgetbakers.couchdb.model.CouchCredentials
import com.budgetbakers.scripts.Credentials

fun Credentials.toCouchCredentials(): CouchCredentials {
    return CouchCredentials(this.url!!, this.dbName!!, this.username!!, this.password!!)
}
