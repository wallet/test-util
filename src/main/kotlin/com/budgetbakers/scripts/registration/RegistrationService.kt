package com.budgetbakers.scripts.registration

import Flavor
import Platform
import com.budgetbakers.be.auth.AuthService
import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.be.model.BeRegistration
import com.budgetbakers.be.model.BeRequestMeta
import com.budgetbakers.be.user.UserClient
import com.budgetbakers.couchdb.model.CouchCredentials
import com.budgetbakers.couchdb.model.Document
import com.budgetbakers.couchdb.service.CouchDbService
import com.ribeez.RibeezProtos
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Service


@Service
class RegistrationService {

  private val log = LoggerFactory.getLogger(javaClass)

  @Autowired
  lateinit var profileService: ProfileGenerationService

  @Autowired
  protected lateinit var couchService: CouchDbService

  @Autowired
  lateinit var authService: AuthService

  @Autowired
  lateinit var userClient: UserClient

  @Autowired
  lateinit var applicationContext: ApplicationContext

  fun register(
    profile: BeRegistration = profileService.generate(Flavor.Wallet),
    dumpFileName: String = "wallet-test-template-01.json"): BeRegistration {
    log.info("will register ${profile.email}/${profile.password} with initial DB template: $dumpFileName")
    val dump: List<Document> = couchService.loadDumpFromResource("/template-dumps/${dumpFileName}")
    return registerInternal(profile, dump)
  }

  fun registerWithDocs(
    profile: BeRegistration = profileService.generate(),
    dump: List<Document>): BeRegistration {
    log.info("will register ${profile.email}/${profile.password} with dump containing ${dump.size} docs")
    return registerInternal(profile, dump)
  }

  private fun registerInternal(profile: BeRegistration, dump: List<Document>): BeRegistration {

    val context: TestContext = applicationContext.getBean(TestContext::class.java, profile.flavor)

    authService.register(profile, BeRequestMeta(platform = Platform.Web, version = "2.0.0", flavor = profile.flavor))
    authService.login(context, profile.email, profile.password)
    val replication = userClient.getUser(context).replication
    log.info("""user $profile registered, replication: curl -u "${replication.login}:${replication.token}" "${replication.url}/${replication.dbName}/_changes/include_docs=false"""")
    val dumpTargetCredentials = CouchCredentials(replication.url, replication.dbName, replication.login, replication.token)

    val migratedDump = couchService.migrateDump(dump, dumpTargetCredentials.username)
    couchService.restoreDb(dumpTargetCredentials, migratedDump)

    if (profile.flavor == Flavor.Board) {
      val companyInfoProto = RibeezProtos.Board.CompanyInfo.newBuilder()
      profile.companyInfo!!
      companyInfoProto.name = profile.companyInfo.name
      companyInfoProto.industry = profile.companyInfo.industry.code
      companyInfoProto.role = profile.companyInfo.role.code
      userClient.updateCompanyInfo(companyInfoProto.build(), context)
    }

    log.info("Registered and restored db for ${profile.email}/${profile.password}")

    return profile
  }
}
