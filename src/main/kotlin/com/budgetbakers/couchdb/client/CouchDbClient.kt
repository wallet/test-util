package com.budgetbakers.couchdb.client

import com.budgetbakers.couchdb.model.AllDocsResponse
import com.budgetbakers.couchdb.model.ChangesResponse
import com.budgetbakers.couchdb.model.CouchCredentials
import com.budgetbakers.couchdb.model.Document
import com.budgetbakers.util.http.*
import com.budgetbakers.util.http.returnValidEntity
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

import org.apache.http.client.fluent.Request
import org.apache.http.entity.ContentType
import org.apache.http.util.EntityUtils
import org.springframework.stereotype.Service

@Service
class CouchDbClient(val objectMapper: ObjectMapper) {

    fun purge(credentials: CouchCredentials, purgeRequest: Map<String, List<String>>) {
        val uri = "${credentials.uri}/_purge"
        Request.Post(uri)
                .basicAuth(credentials)
                .bodyString(objectMapper.writeValueAsString(purgeRequest), ContentType.APPLICATION_JSON)
                .execute()
                .returnValidEntity("Purge $uri failed")
    }


    fun allDocs(credentials: CouchCredentials, includeDocs: Boolean = true): List<Document> {
        val allDocsResponse = Request.Get("${credentials.uri}/_all_docs?include_docs=$includeDocs")
                .basicAuth(credentials).execute().returnValidEntity("_all_docs failed")
        return objectMapper.readValue<AllDocsResponse>(EntityUtils.toString(allDocsResponse)).rows
    }

    fun changes(credentials: CouchCredentials, includeDocs: Boolean = true): List<Document> {
        val allDocsResponse = Request.Get("${credentials.uri}/_changes?include_docs=$includeDocs")
                .basicAuth(credentials).execute().returnValidEntity("_all_docs failed")
        return objectMapper.readValue<ChangesResponse>(EntityUtils.toString(allDocsResponse)).results
    }

    fun bulkDocs(couchCredentials: CouchCredentials, documents: List<Document>) {
        val uri = "${couchCredentials.uri}/_bulk_docs"
        val extractDoc: (Document) -> JsonNode = { it.doc }
        val bulkDocsRequest: Map<String, Any> = mapOf(
                "docs" to documents.map(extractDoc),
                "new_edits" to false
        )
        Request.Post(uri)
                .basicAuth(couchCredentials)
                .bodyString(objectMapper.writeValueAsString(bulkDocsRequest), ContentType.APPLICATION_JSON)
                .returnValidEntity("_bulk_docs $uri failed")
    }
}

