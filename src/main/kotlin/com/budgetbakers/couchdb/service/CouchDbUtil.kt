package com.budgetbakers.couchdb.service

import com.budgetbakers.couchdb.model.Document

object CouchDbUtil {

    fun notDesignDoc(doc: Document): Boolean {
        return !isDesignDoc(doc)
    }

    private fun isDesignDoc(doc: Document): Boolean {
        return doc.id.startsWith("_design")
    }

    fun docsOnly(document: Document): Boolean {
        return notDeleted(document) && notDesignDoc(document)
    }

    private fun notDeleted(document: Document): Boolean {
        return document.deleted != true
    }
}
