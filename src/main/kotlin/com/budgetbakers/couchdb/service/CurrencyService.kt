package com.budgetbakers.couchdb.service

import org.ektorp.CouchDbConnector
import org.ektorp.ViewQuery
import org.springframework.stereotype.Service


@Service
class CurrencyService {
    fun setReferentialCurrency(db: CouchDbConnector, currencyCode: String) {
        val query = ViewQuery().allDocs().includeDocs(true)
        val result = db.queryView(query, Map::class.java)
        val referentialCurrency = result.first { it["reservedModelType"] == "Currency" && it["referential"] == true }
        assert(referentialCurrency != null) { "referential currency not found" }
        val res = referentialCurrency.toMutableMap()
        res["code"] = currencyCode
        db.update(res)
    }
}
