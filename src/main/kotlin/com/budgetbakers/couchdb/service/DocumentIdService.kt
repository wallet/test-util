package com.budgetbakers.couchdb.service

import com.budgetbakers.couchdb.model.Document
import com.budgetbakers.util.UuidService
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import org.springframework.stereotype.Service

@Service
class DocumentIdService(val uuidService: UuidService) {

    private val idStructureRegex = """^(-?[^_]+)(.*)$""".toRegex()

    fun replaceValue(obj: ObjectNode, from: String, to: String) {
        for (field in obj.fields()) {
            when (val fieldValue = field.value) {
                is TextNode -> if (fieldValue.textValue() == from) obj.put(field.key, to)
                is ObjectNode -> replaceValue(fieldValue, from, to)
            }
        }
    }

    fun regenerateIds(origDocuments: List<Document>): List<Document> {
        return origDocuments.map { docMeta ->
            val origId = docMeta.id
            val newId = generateId(origId)
            origDocuments.forEach { replaceValue(it.doc as ObjectNode, origId, newId) }
            docMeta.copy(id = newId)
        }
    }

    fun generateId(id: String): String {
        val matchResult = idStructureRegex.find(id)
        return if (matchResult != null) {
            val (idType, hash) = matchResult.destructured
            if (hash != "") "${idType}_${uuidService.uuid()}" else /* UserConfigure etc. */ id
        } else {
            // id == _design/security etc
            id
        }
    }

    fun setReservedOwnerId(reservedAuthorId: String): (Document) -> Document {
        return { document ->
            (document.doc as ObjectNode)
                    .put("reservedAuthorId", reservedAuthorId)
                    .put("reservedOwnerId", reservedAuthorId)
            document
        }
    }
}
