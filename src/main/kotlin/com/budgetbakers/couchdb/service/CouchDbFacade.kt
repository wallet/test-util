package com.budgetbakers.couchdb.service

import com.budgetbakers.couchdb.model.CouchCredentials
import com.ribeez.RibeezProtos.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CouchDbFacade {

    @Autowired
    lateinit var currencyService: CurrencyService

    @Autowired
    lateinit var couchDbService: CouchDbService

    fun setReferentialCurrency(user: User, currencyCode: String) {
        val couchCredentials = CouchCredentials(user.replication.url, user.replication.dbName, user.replication.login, user.replication.token)
        val db = couchDbService.buildCouchDbConnector(couchCredentials)
        currencyService.setReferentialCurrency(db, currencyCode)
    }
}
