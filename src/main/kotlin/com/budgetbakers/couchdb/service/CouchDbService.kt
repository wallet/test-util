package com.budgetbakers.couchdb.service

import com.budgetbakers.couchdb.client.CouchDbClient
import com.budgetbakers.couchdb.model.AllDocsResponse
import com.budgetbakers.couchdb.model.ChangesResponse
import com.budgetbakers.couchdb.model.CouchCredentials
import com.budgetbakers.couchdb.model.Document
import com.budgetbakers.couchdb.service.CouchDbUtil.docsOnly
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.ektorp.CouchDbConnector
import org.ektorp.http.StdHttpClient
import org.ektorp.impl.StdCouchDbInstance
import org.springframework.stereotype.Service
import java.io.File


@Service
class CouchDbService(
        val client: CouchDbClient,
        val objectMapper: ObjectMapper,
        val documentIdService: DocumentIdService) {


    fun purgeDb(credentials: CouchCredentials) {
        val purgeRequest = client
                .changes(credentials)
                .filter(CouchDbUtil::notDesignDoc)
                .map { docMeta -> docMeta.id to docMeta.changes!!.map { change -> change.rev } }.toMap()
        client.purge(credentials, purgeRequest)
    }

    fun restoreDb(couchCredentials: CouchCredentials, documents: List<Document>) {
        client.bulkDocs(couchCredentials, documents)
    }

    fun loadDumpFromFile(fileName: String): List<Document> {
        return loadDump(File(fileName).readText())
    }

    fun loadDumpFromResource(resourceName: String): List<Document> {
        return loadDump(javaClass.getResource(resourceName).readText())
    }

    fun loadDump(resource: String): List<Document> {
        return (try {
            // curl _changes?include_docs=true response
            objectMapper.readValue<ChangesResponse>(resource).results
        } catch (e: Exception) {
            try {
                objectMapper.readValue<AllDocsResponse>(resource).rows
            } catch (e: Exception) {
                // this service dump - only documents in JSON array
                objectMapper.readValue<List<Document>>(resource)
            }
        }).filter(::docsOnly)
    }

    fun migrateDump(documents: List<Document>, ownerId: String): List<Document> {
        // TODO remove revisions? is it necessary?
        return documentIdService.regenerateIds(documents)
                .map(documentIdService.setReservedOwnerId(ownerId))
    }

    fun dump(credentials: CouchCredentials): List<Document> {
        return client.allDocs(credentials).filter(::docsOnly)
    }

    fun buildCouchDbConnector(credentials: CouchCredentials): CouchDbConnector {
        val httpClient = StdHttpClient.Builder()
                .url(credentials.url)
                .username(credentials.username)
                .password(credentials.password)
                .build()

        val dbInstance = StdCouchDbInstance(httpClient)
        return dbInstance.createConnector(credentials.dbName, false)
    }
}
