package com.budgetbakers.couchdb.model

data class ChangesResponse(val results: List<Document>)
