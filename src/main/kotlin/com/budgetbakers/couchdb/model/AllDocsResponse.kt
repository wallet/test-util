package com.budgetbakers.couchdb.model

data class AllDocsResponse(val rows: List<Document>)
