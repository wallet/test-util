package com.budgetbakers.couchdb.model

data class Change(val rev: String)
