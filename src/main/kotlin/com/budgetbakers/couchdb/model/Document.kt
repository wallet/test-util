package com.budgetbakers.couchdb.model

import com.fasterxml.jackson.databind.JsonNode

data class Document(
        val id: String,
        val deleted: Boolean?,
        val changes: List<Change>?,
        val doc: JsonNode)

