package com.budgetbakers.couchdb.model

data class CouchCredentials(val url: String, val dbName: String, val username: String, val password: String) {

    val token: String = "$username:$password"

    val uri: String = "$url/$dbName"
}
