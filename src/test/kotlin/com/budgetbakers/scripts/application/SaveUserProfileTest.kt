package com.budgetbakers.scripts.application

import Flavor
import com.budgetbakers.be.auth.TestContext
import com.ribeez.RibeezProtos
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.joda.time.DateTime
import org.testng.Assert
import org.testng.annotations.Test

class SaveUserProfileTest : AbstractApplicationTest() {

  @Test(enabled = false)
  fun couchbaseMigrationStart() = runBlocking {
    log.info("After login & get user")
    val user = super.ribeezUser
    val updatedUser1 = RibeezProtos.User.newBuilder(user)
      .setFullName("Updated 1")
      .setFirstName("Updated 1")
      .setLastName("Updated 1")
      .clearLastName()
      .setReplicationCouchbase(
        RibeezProtos.CouchbaseReplicationEndpoint.newBuilder()
          .setLogin("sss")
          .setPassword("ppp")
          .setMigrationFinished(true)
          .setUrl("url")
          .build()
      )
      .build()

    log.info("Saving user ${user.userId} - start.")
    val start = System.currentTimeMillis()
    val saveResult = GlobalScope.async {
      saveUserAsync(updatedUser1, httpContext)
    }
    userClient.migrationStart(user, httpContext)
    log.info("Saving user ${user.userId} - done. ${System.currentTimeMillis() - start}")
    saveResult.await()
    log.info("Saving user ${user.userId} - done. ${System.currentTimeMillis() - start}")
    val refreshedUser = userClient.getUser(httpContext)
    log.info("loaded saved user ${refreshedUser.firstName}, ${refreshedUser.userId}, ${refreshedUser.hasReplicationCouchbase()}")
  }

  @Test(enabled = true)
  fun updateProfile() {
    val user = this.ribeezUser
    val updatedUser = RibeezProtos.User.newBuilder(user)
      .setFirstName("Updated fn 1")
      .setLastName("Updated ln 1")
      .setFullName("Updated full name")
      .setAvatarUrl("http://myavatar.com/pic.png")
      .build()

    userClient.saveUser(updatedUser, httpContext)
    val refreshedUser = userClient.getUser(httpContext)
    Assert.assertEquals(refreshedUser.firstName, "Updated fn 1")
    Assert.assertEquals(refreshedUser.lastName, "Updated ln 1")
    Assert.assertEquals(refreshedUser.fullName, "Updated full name")
    Assert.assertEquals(refreshedUser.avatarUrl, "http://myavatar.com/pic.png")
  }

  @Test(enabled = true)
  fun saveEmptyProfile() {
    val user = this.ribeezUser
    val updatedUser = RibeezProtos.User.newBuilder(user)
      .setFirstName("")
      .setLastName("")
      .setFullName("")
      .setLocation("")
      .setAvatarUrl("")
      .build()
    userClient.saveUser(updatedUser, httpContext)
    val refreshedUser = userClient.getUser(httpContext)
    Assert.assertEquals(refreshedUser.firstName, "")
    Assert.assertEquals(refreshedUser.lastName, "")
    Assert.assertEquals(refreshedUser.fullName, "")
    Assert.assertEquals(refreshedUser.location, "")
    Assert.assertEquals(refreshedUser.avatarUrl, "")
  }

  @Test(enabled = true)
  fun saveClearProfile() {
    val user = this.ribeezUser
    val updatedUser = RibeezProtos.User.newBuilder(user)
      .clearFirstName()
      .clearLastName()
      .clearFullName()
      .clearLocation()
      .clearAvatarUrl()
      .build()
    userClient.saveUser(updatedUser, httpContext)
    val refreshedUser = userClient.getUser(httpContext)
    val userProfile = this.user
    Assert.assertEquals(refreshedUser.firstName, userProfile.firstName)
    Assert.assertEquals(refreshedUser.lastName, userProfile.lastName)
    Assert.assertEquals(refreshedUser.fullName, "${userProfile.firstName} ${userProfile.lastName}")
  }

  @Test(enabled = true)
  fun saveReferral() {
    val userProfile = this.user
    log.info("Update profile with user ${userProfile.email}")
    val referralUserCtx = this.httpContext
    val referralUrl = userClient.getReferralUrl(referralUserCtx)
    val referralId = referralUrl.replace("^.*/".toRegex(), "")

    val invitedUser = registrationService.register()
    val invitedUserCtx = newHttpContext(Flavor.Wallet)
    authService.login(invitedUserCtx, invitedUser.email, invitedUser.password)

    val updatedInvitedUser = RibeezProtos.User.newBuilder(userClient.getUser(invitedUserCtx))
      .setReferralContent("")
      .setReferralMedium("")
      .setReferralId("")
      .setReferralTerm("")
      .build()
    userClient.saveUser(updatedInvitedUser, referralUserCtx)


    val updatedInvitedUser2 = RibeezProtos.User.newBuilder(userClient.getUser(invitedUserCtx))
      .setReferralContent("a")
      .setReferralMedium("b")
      .setReferralId(referralId)
      .setReferralTerm("t")
      .build()
    userClient.saveUser(updatedInvitedUser2, referralUserCtx)
  }

  @Test(enabled = true)
  fun saveWalletVerifiedProfile() {
    userClient.saveVerifiedProfile(httpContext, verifiedProfile(Flavor.Wallet))
  }

  @Test(enabled = true, dependsOnMethods = ["saveWalletVerifiedProfile"])
  fun registerBoard() {
    registerAndLoginFlavor(Flavor.Board)
  }

  @Test(enabled = true, dependsOnMethods = ["registerBoard"])
  fun saveBoardVerifiedProfile() {
    userClient.saveVerifiedProfile(httpContext, verifiedProfile(Flavor.Board))
  }

  @Test(enabled = true, dependsOnMethods = ["saveBoardVerifiedProfile"])
  fun exportVerifiedProfiles() {
    val today = DateTime()
    val textContent = userClient.exportVerifiedProfile(today, today)
    log.info("exportVerifiedProfile content:\n$textContent")
  }

  private fun verifiedProfile(flavor: String): RibeezProtos.VerifiedProfile {
    return RibeezProtos.VerifiedProfile.newBuilder()
      .setBirthday(DateTime().minusYears(18).millis)
      .setCompanyName("Company name verified")
      .setCompanyId("CompanyId verified")
      .setFullName("Full name $flavor verified")
      .setStreetAddress("Street address $flavor verified")
      .build()
  }

  private suspend fun saveUserAsync(user: RibeezProtos.User, context: TestContext) {
    userClient.saveUser(user, context)
  }
}
