package com.budgetbakers.scripts.application

import com.budgetbakers.couchdb.model.CouchCredentials
import com.budgetbakers.couchdb.model.Document
import com.budgetbakers.scripts.CredentialsProvider
import com.budgetbakers.scripts.registration.toCouchCredentials
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import java.io.File
import java.lang.RuntimeException


class PurgeDatabaseTest : AbstractApplicationTest() {

    @Autowired
    private lateinit var credentialsProvider: CredentialsProvider

    lateinit var credentials: CouchCredentials
    lateinit var dumpFileName: String

    // !!! This Couch DB must be prepared, it will be purged and overwritten !!!
    // TODO org.ektorp.impl.StdCouchDbInstance#createConnector might create a new empty DB
    // curl -u "86ef93e2-770e-42fd-9767-016340bcc8ce:656007f1-9eab-430b-b2d9-86c90ee3594a" "https://couch-dev-frankfurt.budgetbakers.com/bb-86ef93e2-770e-42fd-9767-016340bcc8ce/_changes?include_docs=true"
    val dumpTargetCredentials = CouchCredentials("https://couch-dev-frankfurt.budgetbakers.com", "bb-86ef93e2-770e-42fd-9767-016340bcc8ce", "86ef93e2-770e-42fd-9767-016340bcc8ce", "656007f1-9eab-430b-b2d9-86c90ee3594a")

    @BeforeClass
    fun initTest() {
        credentials = credentialsProvider.account1!!.toCouchCredentials()
        dumpFileName = "$projectRoot/dumps/dump-${credentials.dbName}-${System.currentTimeMillis()}.json"
    }

    @Test(enabled = false)
    fun dumpDb() {
        val documents: List<Document> = couchService.dump(credentials)
        val dump = objectMapper.writeValueAsString(documents)
        File(dumpFileName).writeText(dump)
        log.info("Dump ${credentials.dbName} saved to $dumpFileName")
    }

    @Test(enabled = false)
    fun purgeDatabase() {
        couchService.purgeDb(dumpTargetCredentials)
        println("DB ${dumpTargetCredentials.dbName} purged")
    }

    @Test(enabled = false, dependsOnMethods = ["purgeDatabase", "dumpDb"])
    fun restore() {
        val dump = couchService.loadDumpFromFile(dumpFileName)
        val migratedDump = couchService.migrateDump(dump, dumpTargetCredentials.username)
        couchService.restoreDb(dumpTargetCredentials, migratedDump)
    }
}
