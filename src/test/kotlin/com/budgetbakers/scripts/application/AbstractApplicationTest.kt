package com.budgetbakers.scripts.application

import Flavor
import com.budgetbakers.be.auth.AuthService
import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.be.model.BeRegistration
import com.budgetbakers.be.user.UserClient
import com.budgetbakers.couchdb.service.CouchDbService
import com.budgetbakers.scripts.registration.ProfileGenerationService
import com.budgetbakers.scripts.registration.RegistrationService
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ribeez.RibeezProtos.User
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests
import org.testng.annotations.BeforeClass
import java.io.File
import java.nio.file.Paths
import javax.annotation.PostConstruct


@TestExecutionListeners(MockitoTestExecutionListener::class)
@SpringBootTest(classes = [ScriptsApplication::class])
@EnableConfigurationProperties
abstract class AbstractApplicationTest : AbstractTestNGSpringContextTests() {


  /*
  TODO mocked UUID service + possibly time service
  TODO test util beans like e2e-test user generation
  @Configuration
  @Import(Application.class) // the actual configuration
  public static class TestConfig
  {
      @Bean
      public IMyService myService()
      {
          return new MockedMyService();
      }
  }
  */


  protected val objectMapper: ObjectMapper = jacksonObjectMapper()
    .enable(SerializationFeature.INDENT_OUTPUT)
    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
    .setSerializationInclusion(JsonInclude.Include.NON_NULL)

  @Autowired
  protected lateinit var couchService: CouchDbService

  @Autowired
  protected lateinit var registrationService: RegistrationService

  @Autowired
  protected lateinit var authService: AuthService

  @Autowired
  protected lateinit var userClient: UserClient

  @Autowired
  protected lateinit var profileService: ProfileGenerationService

  protected val projectRoot = Paths.get("").toAbsolutePath().toString()

  protected val log: Logger = LoggerFactory.getLogger(javaClass)

  protected lateinit var httpContext: TestContext

  @PostConstruct
  protected fun initBbTests() {
    File("dumps").also { it.mkdirs() }
  }

  @BeforeClass
  protected fun registerAndLogin() {
    registerAndLoginFlavor(Flavor.Wallet)
  }

  protected lateinit var user: BeRegistration

  protected lateinit var ribeezUser: User

  protected fun newHttpContext(flavor: String): TestContext {
    return applicationContext!!.getBean(TestContext::class.java, flavor)
  }

  protected fun registerAndLoginFlavor(flavor: String) {
    log.info("AbstractBraintreeTest#registerAndLoginFlavor($flavor)")
    httpContext = newHttpContext(flavor)
    user = if (flavor == Flavor.Board) {
      registrationService.register(
        profile = profileService.generate(Flavor.Board),
        dumpFileName = "board-test-template-01.json"
      )
    } else {
      registrationService.register()
    }

    authService.login(httpContext, user.email, user.password)
    ribeezUser = userClient.getUser(httpContext)
  }
}
