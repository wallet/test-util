package com.budgetbakers.scripts.application

import com.budgetbakers.couchdb.model.AllDocsResponse
import com.budgetbakers.couchdb.service.DocumentIdService
import com.budgetbakers.util.UuidService
import com.fasterxml.jackson.module.kotlin.readValue
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.testng.Assert
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test


class ReplaceIdTest : AbstractApplicationTest() {

    @Autowired
    private lateinit var documentIdService: DocumentIdService

    @MockBean
    private lateinit var mockUuidService: UuidService

    private val inputJson = """
        {"rows":[
        {"id": "-Record_rec1", "doc": {"_id": "-Record_rec1", "accountId": "-Account_acc1", "ext": {"currId": "-Currency_curr1"}}},
        {"id": "-Currency_curr1", "doc": {"_id": "-Currency_curr1"}},
        {"id": "-Account_acc1", "doc": {"_id": "-Account_acc1"}},
        {"id": "UserConfigure", "doc": {"_id": "UserConfigure", "type": 0}},
        {"id": "-HashTag_hash-tag-1", "doc": {"_id": "-HashTag_hash-tag-1", "name": "Label 1"}}
        ]}
        """.trimIndent()

    @BeforeMethod
    fun init() {
        Mockito.reset(mockUuidService)
        var mockedUuid = Mockito.`when`(mockUuidService.uuid())
        for (idSeq in 1..100) {
            mockedUuid = mockedUuid.thenReturn("mock-id-$idSeq")
        }
    }

    @Test(enabled = false)
    fun test2() {
        val recordId = "-Record_aaa-bbb-cccc"
        Assert.assertEquals(documentIdService.generateId(recordId), "-Record_mock-id-1")

        val labelId = "-HashTag_ggg-hhh-iii"
        Assert.assertEquals(documentIdService.generateId(labelId), "-HashTag_mock-id-2")

        val userConfigure = "-UserConfigure"
        Assert.assertEquals(documentIdService.generateId(userConfigure), "-UserConfigure")
    }

    @Test(enabled = false)
    fun test1() {
        val origDocuments = objectMapper.readValue<AllDocsResponse>(inputJson).rows
        val result = documentIdService.regenerateIds(origDocuments)
        log.info("result=${objectMapper.writeValueAsString(result)}")
    }
}
