package com.budgetbakers.scripts.application

import Flavor
import com.budgetbakers.be.imports.ImportsClient
import com.budgetbakers.be.model.BeRegistration
import com.budgetbakers.be.user.UserClient
import com.ribeez.RibeezProtos
import org.springframework.beans.factory.annotation.Autowired
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

class ImportsTest : AbstractApplicationTest() {

  @Autowired
  lateinit var importsClient: ImportsClient

  lateinit var credentials: BeRegistration


  @Test(enabled = false)
  fun sendInfoEmail() {
    importsClient.sendInfoEmail(httpContext)
  }

  @Test(enabled = false, dependsOnMethods = ["login"])
  fun getUserImports() {
    val imports = importsClient.getUserImports(httpContext)
    println("imports=${imports.itemsCount}")
  }
}
