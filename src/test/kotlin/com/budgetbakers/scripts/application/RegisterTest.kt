package com.budgetbakers.scripts.application

import Flavor
import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.be.model.BeRegistration
import org.testng.annotations.Test

class RegisterTest : AbstractApplicationTest() {

  lateinit var credentials: BeRegistration

  override fun registerAndLogin() {
    // don't register default user in registration test
  }

  @Test(enabled = true)
  fun registerWallet() {
    credentials = registrationService.register(
      profile = profileService.generate(Flavor.Wallet),
      dumpFileName = "wallet-test-template-01.json"
    )
    log.info("User ${credentials.email} registered.")
  }

  @Test(enabled = true, dependsOnMethods = ["registerWallet"])
  fun loginWallet() {
    val httpContext = newHttpContext(Flavor.Wallet)
    authService.login(httpContext, credentials.email, credentials.password)
    ribeezUser = userClient.getUser(httpContext)
    logUserInfo()
  }

  @Test(enabled = true, dependsOnMethods = ["loginWallet"])
  fun registerBoard() {
    credentials = registrationService.register(
      profile = profileService.generate(Flavor.Board),
      dumpFileName = "board-test-template-01.json"
    )
    log.info("User ${credentials.email} registered.")
  }

  @Test(enabled = true, dependsOnMethods = ["registerBoard"])
  fun loginBoard() {
    val httpContext: TestContext = newHttpContext(Flavor.Board)
    authService.login(httpContext, credentials.email, credentials.password)
    ribeezUser = userClient.getUser(httpContext)
    logUserInfo()
  }

  private fun logUserInfo() {
    log.info("User email=${ribeezUser.email}, fullName=${ribeezUser.fullName} loaded, CouchDB: ${ribeezUser.replication.url}")
  }
}
