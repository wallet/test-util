package com.budgetbakers.scripts.application

import com.budgetbakers.be.mobile.WakeUpAcknowledgeClient
import com.budgetbakers.be.model.BeRegistration
import com.budgetbakers.util.UuidService
import com.ribeez.RibeezProtos
import org.springframework.beans.factory.annotation.Autowired
import org.testng.annotations.Test

class AppInstallationTest : AbstractApplicationTest() {

//  @Autowired
//  lateinit var userClient: UserClient

  @Autowired
  lateinit var mobileAppClient: WakeUpAcknowledgeClient

  @Autowired
  lateinit var uuidService: UuidService

  lateinit var credentials: BeRegistration
//  lateinit var user: RibeezProtos.User

//  @BeforeClass
//  fun initClass() {
//    httpContext = newHttpContext(Flavor.Wallet)
//    credentials = BeRegistration(email = "e2e-test-dc8fce60@getnada.com", password = "qqqqqq", flavor = httpContext.flavor) // notas 4
//    authService.login(httpContext, credentials.email, credentials.password)
//    user = userClient.getUser(httpContext)
//  }

  @Test(enabled = false)
  fun install() {
    val installation = RibeezProtos.Installation.newBuilder()
      .setInstallationId(uuidService.uuid())
      .setUserId(ribeezUser.userId)
      .setCreatedAt(System.currentTimeMillis())
      .setUpdatedAt(System.currentTimeMillis())
      .setPlatform(RibeezProtos.PlatformType.ANDROID)
      .setAppVersionCode(81000)
      .setAppVersionName("8.0.0.0")
      .build()
    mobileAppClient.install(httpContext, installation)
  }

  @Test(enabled = false)
  fun acknowledge() {
    mobileAppClient.acknowledge(httpContext)
  }

  @Test(enabled = false)
  fun initJobs() {
    mobileAppClient.initJobs()
  }
}
