package com.budgetbakers

import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.scripts.application.AbstractApplicationTest
import com.ribeez.RibeezProtos
import org.testng.Assert
import org.testng.annotations.Test

class UserReferralTest : AbstractApplicationTest() {

  private lateinit var mallPayContext: TestContext

  @Test
  fun setKbReferral() {
    registerAndLoginFlavor(Flavor.Wallet)
    userClient.updateReferralSource(RibeezProtos.ReferralSource.KB, httpContext)
    val user = userClient.getUser(httpContext)
    Assert.assertTrue(user.hasReferralSource())
    Assert.assertEquals(user.referralSource, RibeezProtos.ReferralSource.KB)
  }

  @Test
  fun setMallPayReferral() {
    mallPayContext = httpContext
    registerAndLoginFlavor(Flavor.Wallet)
    userClient.updateReferralSource(RibeezProtos.ReferralSource.MALL_PAY, mallPayContext)
    val user = userClient.getUser(mallPayContext)
    Assert.assertTrue(user.hasReferralSource())
    Assert.assertEquals(user.referralSource, RibeezProtos.ReferralSource.MALL_PAY)
  }

  @Test(dependsOnMethods = ["setMallPayReferral"])
  fun dontOverwriteReferral() {
    userClient.updateReferralSource(RibeezProtos.ReferralSource.KB, mallPayContext)
    val user = userClient.getUser(mallPayContext)
    Assert.assertTrue(user.hasReferralSource())
    Assert.assertEquals(user.referralSource, RibeezProtos.ReferralSource.MALL_PAY)
  }
}
