package com.budgetbakers.groupsharing

import com.budgetbakers.be.group.GroupSharingClient
import com.budgetbakers.scripts.application.AbstractApplicationTest
import org.springframework.beans.factory.annotation.Autowired

abstract class AbstractGroupSharingTest : AbstractApplicationTest() {

  @Autowired
  protected lateinit var groupSharingClient: GroupSharingClient

}
