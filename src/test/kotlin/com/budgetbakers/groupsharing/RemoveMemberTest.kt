package com.budgetbakers.groupsharing

import Flavor
import com.budgetbakers.be.auth.TestContext
import com.budgetbakers.be.group.model.Group
import com.budgetbakers.be.group.model.GroupMember
import com.budgetbakers.be.model.BeRegistration
import org.testng.Assert
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

class RemoveMemberTest : AbstractGroupSharingTest() {

  private lateinit var groupOwner1: BeRegistration
  private lateinit var groupOwnerUserId1: String
  private lateinit var groupOwnerContext1: TestContext

  private lateinit var groupOwner2: BeRegistration
  private lateinit var groupOwnerUserId2: String
  private lateinit var groupOwnerContext2: TestContext

  private fun createGroup(owner: BeRegistration, ownerHttpContext: TestContext, member1: BeRegistration, member2: BeRegistration): String {
    val existingGroupUser1 = groupSharingClient.getGroupUser(httpContext, member1.email)
    val existingGroupUser2 = groupSharingClient.getGroupUser(httpContext, member2.email)
    val memberRequest1 = GroupMember.Builder.from(existingGroupUser1)
    val memberRequest2 = GroupMember.Builder.from(existingGroupUser2)
    val createGroupRequest = Group("Test group ${owner.email}", listOf(memberRequest1, memberRequest2))
    groupSharingClient.submitGroup(ownerHttpContext, createGroupRequest.toCreateProto())
    val ribeezUser = userClient.getUser(ownerHttpContext)
    log.info("Group for owner userId=${ribeezUser.userId}, email=${owner.email} with member emails=[${member1.email}, ${member2.email}] created")
    return ribeezUser.userId
  }

  @BeforeClass
  fun createGroups() {
    // the first member is default test user
    registerAndLogin()
    val groupMember1 = user

    // create the second member
    val groupMember2 = profileService.generate(Flavor.Wallet)
    registrationService.register(groupMember2)

    groupOwner1 = profileService.generate(Flavor.Wallet)
    registrationService.register(groupOwner1)
    groupOwnerContext1 = newHttpContext(groupOwner1.flavor)
    authService.login(groupOwnerContext1, groupOwner1.email, groupOwner1.password)
    groupOwnerUserId1 = createGroup(groupOwner1, groupOwnerContext1, groupMember1, groupMember2)


    groupOwner2 = profileService.generate(Flavor.Wallet)
    registrationService.register(groupOwner2)
    groupOwnerContext2 = newHttpContext(groupOwner2.flavor)
    authService.login(groupOwnerContext2, groupOwner2.email, groupOwner2.password)
    groupOwnerUserId2 = createGroup(groupOwner2, groupOwnerContext2, groupMember1, groupMember2)
  }

  @Test
  fun removeFromAllGroups() {
    val group1before = groupSharingClient.getGroup(groupOwnerContext1)
    val group2before = groupSharingClient.getGroup(groupOwnerContext2)
    userClient.deleteUser(httpContext)
    val group1after = groupSharingClient.getGroup(groupOwnerContext1)
    val group2after = groupSharingClient.getGroup(groupOwnerContext2)
    Assert.assertEquals(group1before.groupMemberCount - group1after.groupMemberCount, 1, "One member should be removed from group1")
    Assert.assertEquals(group2before.groupMemberCount - group2after.groupMemberCount, 1, "One member should be removed from group2")
  }
}
