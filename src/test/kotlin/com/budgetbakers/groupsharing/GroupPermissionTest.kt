package com.budgetbakers.groupsharing

import com.budgetbakers.be.group.model.Group
import com.budgetbakers.be.group.model.GroupAccountPermission
import com.budgetbakers.be.group.model.GroupMember
import com.ribeez.RibeezProtos
import org.testng.Assert
import org.testng.annotations.Test

class GroupPermissionTest : AbstractGroupSharingTest() {

  @Test
  fun inviteUser() {
    val account1ReadOnly = GroupAccountPermission("-AccountId_123", RibeezProtos.GroupAccessPermission.READ_ONLY)
    val account2ReadWrite = GroupAccountPermission("-AccountId_456", RibeezProtos.GroupAccessPermission.READ_WRITE)
    val member1 = GroupMember(email = "sobicek1@getnada.com",modulePermissions =  listOf(account1ReadOnly))
    val member2 = GroupMember(email = "sobicek2@getnada.com", modulePermissions = listOf(account2ReadWrite))
    val group = Group("Test group", listOf(member1, member2))
    groupSharingClient.submitGroup(httpContext, group.toCreateProto())
  }

  @Test(dependsOnMethods = ["inviteUser"])
  fun getGroup() {
    val group = groupSharingClient.getGroup(httpContext)
    val member1 = group.groupMemberAnonymousList[0]
    Assert.assertEquals(1, member1.modulePermissionList.size)
    val member1Module1Permissions = member1.modulePermissionList[0]
    Assert.assertTrue(member1Module1Permissions.hasModelType())
    Assert.assertEquals(member1Module1Permissions.modelType, RibeezProtos.ModelType.Account)
  }
}
