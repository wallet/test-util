package com.budgetbakers.groupsharing

import Flavor
import com.budgetbakers.be.group.model.Group
import com.budgetbakers.be.group.model.GroupMember
import com.budgetbakers.be.model.BeRegistration
import org.testng.Assert
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

class LeaveGroupTest : AbstractGroupSharingTest() {

  private lateinit var anonymousMember: BeRegistration
  private lateinit var groupMember2: BeRegistration

  @BeforeClass
  fun createGroup() {
    anonymousMember = profileService.generate(Flavor.Wallet)
    val memberRequest1 = GroupMember(email = anonymousMember.email)

    groupMember2 = profileService.generate(Flavor.Wallet)
    registrationService.register(groupMember2)
    val existingGroupUser1 = groupSharingClient.getGroupUser(httpContext, groupMember2.email)
    val memberRequest2 = GroupMember.Builder.from(existingGroupUser1)

    val createGroupRequest = Group("Test group", listOf(memberRequest1, memberRequest2))
    groupSharingClient.submitGroup(httpContext, createGroupRequest.toCreateProto())
    val group = groupSharingClient.getGroup(httpContext)
    Assert.assertEquals(group.groupMemberAnonymousCount, 1, "There should be one anonymous user.")
    val member1 = group.getGroupMemberAnonymous(0)
    Assert.assertEquals(group.groupMemberCount, 1, "There should be one registered member.")
    val member2 = group.getGroupMember(0).user
    val owner = group.groupOwner.user
    log.info("Created group=${group.name} for owner=[${owner.userId}, ${owner.email}] and anonymous members=[${member1.email}], members = [${member2.userId}, ${member2.email}]")
  }

  @Test
  fun leaveGroup() {
    log.info("leaveGroup owner=${user.email} members=[${anonymousMember.email}, ${groupMember2.email}]")
    val memberContext = newHttpContext(groupMember2.flavor)
    authService.login(memberContext, groupMember2.email, groupMember2.password)
    groupSharingClient.leaveGroup(memberContext, ribeezUser.userId)
    val group = groupSharingClient.getGroup(httpContext)
    Assert.assertEquals(group.groupMemberAnonymousCount, 1, "Anonymous user should remain.")
    Assert.assertEquals(group.groupMemberCount, 0, "There should be no registered member.")
  }

  @Test(dependsOnMethods = ["leaveGroup"])
  fun rejoinGroup() {
    val groupProto = groupSharingClient.getGroup(httpContext)
    val originalGroup = Group.Builder.from(groupProto)
    val existingGroupUser1 = groupSharingClient.getGroupUser(httpContext, groupMember2.email)
    val createGroupRequest = originalGroup.addMember(existingGroupUser1)
    groupSharingClient.submitGroup(httpContext, createGroupRequest.toCreateProto())
    val updatedGroup = groupSharingClient.getGroup(httpContext)
    Assert.assertEquals(updatedGroup.groupMemberAnonymousCount, 1, "There should be one anonymous user.")
    Assert.assertEquals(updatedGroup.groupMemberAnonymousList[0].email, anonymousMember.email)
    Assert.assertEquals(updatedGroup.groupMemberCount, 1, "There should be one registered member.")
    Assert.assertEquals(updatedGroup.groupMemberList[0].user.email, groupMember2.email)
  }
}
