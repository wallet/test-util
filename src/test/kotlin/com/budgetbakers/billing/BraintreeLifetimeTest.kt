package com.budgetbakers.billing

import com.ribeez.RibeezBillingProtos
import org.testng.Assert
import org.testng.annotations.Test


class BraintreeLifetimeTest : AbstractBraintreeTest() {


    private lateinit var lifetimeProduct: RibeezBillingProtos.BraintreeProduct

    @Test(enabled = true)
    fun getProducts() {
        val product = braintreeClient.getProducts(httpContext)
                .recurrentProductsList.find { it.product.period == RibeezBillingProtos.Period.MONTH }
        Assert.assertNotNull(product, "Monthly product not found")
        log.info("Monthly product price = ${product!!.price}, ${product.currencyCode}")
        lifetimeProduct = product
    }

    @Test(enabled = true, dependsOnMethods = ["getProducts"])
    fun createLifetime() {
        val transaction = RibeezBillingProtos.Transaction.newBuilder()
                .setToken("fake-valid-no-billing-address-nonce")
                .setProduct(lifetimeProduct.product)
                .setTransactionId("")
                .build()
        braintreeClient.createLifetime(httpContext, transaction)
    }
}
