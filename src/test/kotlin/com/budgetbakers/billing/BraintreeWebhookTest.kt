package com.budgetbakers.billing


import Flavor
import com.budgetbakers.be.dev.DevFeatureClient
import com.budgetbakers.mock.braintree.Customer
import com.budgetbakers.mock.braintree.Subject
import com.budgetbakers.mock.braintree.Subscription
import com.budgetbakers.mock.braintree.WebhookNotification
import com.budgetbakers.mock.braintree.Transaction  as WebhookTransaction
import com.ribeez.RibeezBillingProtos.*
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.testng.annotations.Test

class BraintreeWebhookTest : AbstractBraintreeTest() {

  @Autowired
  private lateinit var devFeatureClient: DevFeatureClient

  private lateinit var details: BraintreeDetails

  override fun registerAndLogin() {
    // don't register default Wallet account
  }

  @Test
  fun prepareWalletData() {
    prepareData(Flavor.Wallet, Period.YEAR)
  }

  @Test(enabled = true, dependsOnMethods = ["prepareWalletData"])
  fun send1stWalletNotification() {
    subscriptionChargedUnsuccessfullyWebhook(1)
  }

  @Test(enabled = true, dependsOnMethods = ["send1stWalletNotification"])
  fun send2ndWalletNotification() {
    subscriptionChargedUnsuccessfullyWebhook(2)
  }

  @Test(enabled = true, dependsOnMethods = ["send2ndWalletNotification"])
  fun sendWalletPremiumCanceled() {
    sendSubscriptionCanceledWebhook()
  }

  @Test(enabled = true, dependsOnMethods = ["sendWalletPremiumCanceled"])
  fun sendWalletCharged() {
    sendSubscriptionChargedSuccessfullyWebhook()
  }

  @Test(enabled = true, dependsOnMethods = ["sendWalletCharged"])
  private fun prepareBoardData() {
    prepareData(Flavor.Board, Period.MONTH)
  }

  @Test(enabled = true, dependsOnMethods = ["prepareBoardData"])
  fun send1stBoardNotification() {
    subscriptionChargedUnsuccessfullyWebhook(1)
  }

  @Test(enabled = true, dependsOnMethods = ["send1stBoardNotification"])
  fun send2ndBoardNotification() {
    subscriptionChargedUnsuccessfullyWebhook(2)
  }

  @Test(enabled = true, dependsOnMethods = ["send2ndBoardNotification"])
  fun sendBoardPremiumCanceled() {
    sendSubscriptionCanceledWebhook()
  }

  private fun prepareData(flavor: String, period: Period) {
    registerAndLoginFlavor(flavor)
    val products = braintreeClient.getProducts(httpContext)
    val subscriptionProducts = products.recurrentProductsList
    val product = subscriptionProducts.find { it.product.period == period } ?: throw AssertionError("Monthly product not found")

    log.info("Got $period product price = ${product.price}, ${product.currencyCode} for $flavor")

    val transaction = Transaction.newBuilder()
      .setToken("fake-valid-no-billing-address-nonce")
      .setProduct(product.product)
      .setTransactionId("")
      .build()
    braintreeClient.createSubscription(httpContext, transaction)
    details = braintreeClient.getDetails(httpContext)
  }

  private fun subscriptionChargedUnsuccessfullyWebhook(failureCount: Int) {
    val webhook = buildWebhook(details).copy(kind = "subscription_charged_unsuccessfully")
    devFeatureClient.sendMockWebhook(webhook.copy(subject = webhook.subject.copy(subscription = webhook.subject.subscription!!.copy(failureCount = failureCount))))
  }

  private fun sendSubscriptionCanceledWebhook() {
    val webhook = buildWebhook(details).copy(kind = "subscription_canceled")
    // kind from com.braintreegateway.WebhookNotification.Kind
    devFeatureClient.sendMockWebhook(webhook)
  }

  private fun sendSubscriptionChargedSuccessfullyWebhook() {
    val transaction = WebhookTransaction(id = "abc", amount = "100", customer = Customer(id = ribeezUser.userId))
    val webhook = buildWebhook(details).copy(kind = "subscription_charged_successfully")
    // kind from com.braintreegateway.WebhookNotification.Kind
    devFeatureClient.sendMockWebhook(webhook.copy(subject = webhook.subject.copy(subscription = webhook.subject.subscription!!.copy(transactions = listOf(transaction)))))
  }

  private fun buildWebhook(details: BraintreeDetails): WebhookNotification {
    val subscription = details.subscription
    return WebhookNotification(
      kind = "",
      subject = Subject(Subscription(
        id = subscription.id,
        planId = subscription.planId,
        // simulate over due, otherwise BE does not send e-mails
        nextBillingDate = DateTime().minusDays(7).toString("yyyy-MM-dd"),
        nextBillingPeriodAmount = subscription.nextBillingPeriodAmount.toString(),
        merchantAccountId = "budgetbakers${subscription.currencyCode}",
        paymentMethodToken = details.defaultPaymentMethod.token,
        failureCount = 0
      )))
  }
}
