package com.budgetbakers.billing

import com.ribeez.RibeezBillingProtos
import org.testng.Assert
import org.testng.annotations.Test


class BraintreeSubscriptionTest : AbstractBraintreeTest() {

    private lateinit var token: String
    private lateinit var monthlyProduct: RibeezBillingProtos.BraintreeProduct

    @Test(enabled = true)
    fun getDetailsBeforeSubscription() {
        val details = braintreeClient.getDetails(httpContext)
        val p = details.defaultPaymentMethod
//        val s = if (details.hasSubscription())  "yes" else "no"
        println("Details: paymentMethod=${p.type}, ${p.maskedNumber}, ${p.isExpired}, ${p.expirationDate}, ${p.creditCardType}, ${p.payPalEmail}, subscription=${if (details.hasSubscription()) "yes" else "no"}")
    }

    @Test(enabled = true)
    fun getToken() {
        token = braintreeClient.getToken(httpContext)
        Assert.assertTrue(token.length > 2000, "Token should be string more than 2000 characters long but is: '${token}'")
    }

    @Test(enabled = true)
    fun getProducts() {
        val products = braintreeClient.getProducts(httpContext)
        val subscriptionProducts = products.recurrentProductsList
        val monthlyProductOpt = subscriptionProducts.find { it.product.period == RibeezBillingProtos.Period.MONTH }
        Assert.assertNotNull(monthlyProductOpt, "Monthly product not found")
        log.info("Monthly product price = ${monthlyProductOpt!!.price}, ${monthlyProductOpt.currencyCode}")
        monthlyProduct = monthlyProductOpt

        val yearlyProduct = subscriptionProducts.find { it.product.period == RibeezBillingProtos.Period.YEAR }
        log.info("Yearly product price = ${yearlyProduct!!.price}, ${yearlyProduct.currencyCode}")

        if (products.hasLifetimeProduct()) {
            log.info("Lifetime product price = ${products.lifetimeProduct.price}, ${products.lifetimeProduct.currencyCode}")
        } else {
            log.info("No lifetime product")
        }
    }

    @Test(enabled = true, dependsOnMethods = ["getProducts"])
    fun createSubscription() {

        val billingAddress = RibeezBillingProtos.BraintreeBillingAddress.newBuilder()
                .setCompany("company")
                .setCountryCodeAlpha2("CZ")
                .setExtendedAddress("naproti superchromu")
                .setCity("smichov")
                .setPostalCode("15000")
                .setStreetAddress("radlicka 180")
                .setVatNumber("CZ123456")
                .build()
        braintreeClient.updateBillingAddress(httpContext, billingAddress)

        val transaction = RibeezBillingProtos.Transaction.newBuilder()
          .setToken("fake-valid-no-billing-address-nonce")
          .setProduct(monthlyProduct.product)
          .setTransactionId("")
          .build()
        braintreeClient.createSubscription(httpContext, transaction)
    }

    @Test(enabled = true, dependsOnMethods = ["createSubscription"])
    fun getDetailsAfterSubscription() {
        val details = braintreeClient.getDetails(httpContext)
        val p = details.defaultPaymentMethod
        val s = details.subscription
        val b = details.billingAddress
        println("Details: company=${b.company}, vat=${b.vatNumber} paymentMethod=${p.type}, ${p.maskedNumber}, ${p.isExpired}, ${p.expirationDate}, ${p.creditCardType}, ${p.payPalEmail}, subscription=${if (s != null) subscriptionToString(s) else "no"}")
    }

    @Test(enabled = true, dependsOnMethods = ["createSubscription"])
    fun updateBillingAddress() {
        val billingAddress = braintreeClient.getDetails(httpContext).billingAddress
        val updatedAddress = billingAddress.toBuilder()
                .setCompany("Company updated")
                .setCountryCodeAlpha2("US")
                .setExtendedAddress("Updated extended")
                .setCity("City updated")
                .setPostalCode("ZIPupdate")
                .setStreetAddress("Street updated")
                .setVatNumber("VAT updated")
                .build()
        braintreeClient.updateBillingAddress(httpContext, updatedAddress)
    }

    @Test(enabled = true, dependsOnMethods = ["createSubscription"])
    fun getTransaction() {
        val transactions = braintreeClient.getTransaction(httpContext)
        transactions.forEach {
            println("${it.amount}, ${it.currencyCode}, ${it.status}, ${it.paymentMethod.type}, ${it.paymentMethod.creditCardType}, ${it.paymentMethod.expirationDate}, ${it.paymentMethod.isExpired}")
            println("${it.billingStartDate}, ${it.billingEndDate}, ${it.billingAddress.company}, ${it.taxAmount}, ${it.vatNumber}")
        }
    }

    @Test(enabled = true, dependsOnMethods = ["createSubscription"])
    fun updatePaymentMethod() {
        // TODO fake nonces without billing info - protoze to jinak zalozi billing address
        // fake nonces: https://developers.braintreepayments.com/reference/general/testing/ruby
        braintreeClient.updatePaymentMethod(httpContext, "fake-valid-maestro-nonce")
    }

    @Test(enabled = true, dependsOnMethods = ["getTransaction"])
    fun cancelSubscription() {
        braintreeClient.cancelSubscription(httpContext)
    }

    fun subscriptionToString(subscription: RibeezBillingProtos.BraintreeSubscription): String {
        return "Subscription[${subscription.nextBillingPeriodAmount}, ${subscription.currencyCode}, ${subscription.nextBillingDate}]"
    }
}
