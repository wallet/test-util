package com.budgetbakers.billing

import Flavor
import com.budgetbakers.couchdb.service.CouchDbFacade
import com.ribeez.RibeezBillingProtos
import com.ribeez.RibeezProtos
import org.springframework.beans.factory.annotation.Autowired
import org.testng.Assert
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import kotlin.math.abs


class BraintreePriceTest : AbstractBraintreeTest() {


  @Autowired
  lateinit var couchDbFacade: CouchDbFacade

  override fun registerAndLogin() {
    // don't register default account
  }

  @DataProvider(name = "wallet-price-provider")
  fun walletParameters(): Array<Array<Any>> {
    val fallbackPrice = { accountCurrency: String -> arrayOf(accountCurrency, "USD", 4.19, 24.99, 49.99) }
    return arrayOf(
      fallbackPrice("AFN"),
      arrayOf("AUD", "AUD", 5.79, 34.99, 68.99), // Supported currency
      arrayOf("CZK", "CZK", 93.99, 569.99, 1129.0), // Supported currency
      arrayOf("EUR", "EUR", 3.69, 21.99, 43.99), // Supported currency
      arrayOf("GBP", "GBP", 3.29, 19.99, 39.99), // Supported currency
      fallbackPrice("IDR"), // "IDR", 60299.0, 361599.0, 723199.0), // Supported currency
      arrayOf("MYR", "MYR", 17.99, 109.99, 209.99), // Supported currency
      arrayOf("RUB", "RUB", 279.99, 1649.0, 3289.0), // Supported currency
      fallbackPrice("USD") // Supported currency, USD is fallback currency for price
    )
  }

  @Test(enabled = true)
  fun registerWalletAccount() {
    registerAndLoginFlavor(Flavor.Wallet)
  }

  @Test(enabled = true, dataProvider = "wallet-price-provider", dependsOnMethods = ["registerWalletAccount"])
  fun getWalletProducts(accountCurrency: String, priceCurrency: String, monthlyPrice: Double, yearlyPrice: Double, lifetimePrice: Double?) {
    getProducts(accountCurrency, priceCurrency, monthlyPrice, yearlyPrice, lifetimePrice)
  }

  @DataProvider(name = "board-price-provider")
  fun boardParameters(): Array<Array<Any?>> {
    val noLifetime: Double? = null
    val fallbackPrice = { accountCurrency: String -> arrayOf(accountCurrency, "USD", 8.29, 82.99, noLifetime) }
    return arrayOf(
      fallbackPrice("AFN"),
      arrayOf("AUD", "AUD", 11.99, 119.99, noLifetime), // Supported currency
      arrayOf("CZK", "CZK", 189.99, 1879, noLifetime), // Supported currency
      arrayOf("EUR", "EUR", 7.29, 72.99, noLifetime), // Supported currency
      arrayOf("GBP", "GBP", 6.59, 65.99, noLifetime), // Supported currency
      fallbackPrice("IDR"), // "IDR", 120599, 1205999, noLifetime), // Supported currency
      arrayOf("MYR", "MYR", 34.99, 349.99, noLifetime), // Supported currency
      arrayOf("RUB", "RUB", 549.99, 5489, noLifetime), // Supported currency
      fallbackPrice("USD") // Supported currency, USD is fallback currency for price
    )
  }

  @DataProvider(name = "board-kb-price-provider")
  fun boardKbParameters(): Array<Array<Any?>> {
    val defaultParameters = boardParameters()
    assert(defaultParameters.size == 9) { "default Board parameters changed" }
    defaultParameters[0][3] = 61.99 // AFN -> USD
    defaultParameters[1][3] = 85.99 // AUD
    defaultParameters[2][3] = 1409.0 // CZK
    defaultParameters[3][3] = 54.99 // EUR
    defaultParameters[4][3] = 49.99 // GBP
    defaultParameters[5][3] = 61.99 // IDR -> USD
    defaultParameters[6][3] = 259.99 // MYR
    defaultParameters[7][3] = 4119.0 // RUB
    defaultParameters[8][3] = 61.99 // USD
    return defaultParameters
  }

  @Test(enabled = true, dependsOnMethods = ["getWalletProducts"])
  fun registerBoardAccount() {
    registerAndLoginFlavor(Flavor.Board)
  }

  @Test(enabled = true, dataProvider = "board-price-provider", dependsOnMethods = ["registerBoardAccount"])
  fun getBoardProducts(accountCurrency: String, priceCurrency: String, monthlyPrice: Double, yearlyPrice: Double, lifetimePrice: Double?) {
    getProducts(accountCurrency, priceCurrency, monthlyPrice, yearlyPrice, lifetimePrice)
  }

  @Test(enabled = true, dependsOnMethods = ["getBoardProducts"])
  fun setUserReferralToKb() {
    userClient.updateReferralSource(RibeezProtos.ReferralSource.KB, httpContext)
  }

  @Test(enabled = true, dataProvider = "board-kb-price-provider", dependsOnMethods = ["setUserReferralToKb"])
  fun getKbBoardProducts(accountCurrency: String, priceCurrency: String, monthlyPrice: Double, yearlyPrice: Double, lifetimePrice: Double?) {
    getProducts(accountCurrency, priceCurrency, monthlyPrice, yearlyPrice, lifetimePrice)
  }

  private fun getProducts(accountCurrency: String, priceCurrency: String, monthlyPrice: Double, yearlyPrice: Double, lifetimePrice: Double?) {
    couchDbFacade.setReferentialCurrency(ribeezUser, accountCurrency)
    val products = braintreeClient.getProducts(httpContext)

    val subscriptionProducts = products.recurrentProductsList
    val monthlyProduct = subscriptionProducts.find { it.product.period == RibeezBillingProtos.Period.MONTH }!!
    log.info("Monthly product price = ${monthlyProduct.price}, ${monthlyProduct.currencyCode}")
    assertCurrency(monthlyProduct.currencyCode, priceCurrency, "monthly")
    assertPrice(monthlyProduct.price, monthlyPrice, "monthly")

    val yearlyProduct = subscriptionProducts.find { it.product.period == RibeezBillingProtos.Period.YEAR }!!
    log.info("Yearly product price = ${yearlyProduct.price}, ${yearlyProduct.currencyCode}")
    assertCurrency(yearlyProduct.currencyCode, priceCurrency, "yearly")
    assertPrice(yearlyProduct.price, yearlyPrice, "yearly")

    Assert.assertEquals(products.hasLifetimeProduct(), lifetimePrice != null, "unexpected lifetime product")
    if (products.hasLifetimeProduct()) {
      log.info("Lifetime product price = ${products.lifetimeProduct.price}, ${products.lifetimeProduct.currencyCode}")
      assertCurrency(products.lifetimeProduct.currencyCode, priceCurrency, "lifetime")
      assertPrice(products.lifetimeProduct.price, lifetimePrice!!, "Lifetime")
    } else {
      log.info("No lifetime product")
    }
  }

  private fun assertCurrency(actual: String, expected: String, product: String) {
    Assert.assertEquals(actual, expected, "Currency does not match for $product product")
  }

  private fun assertPrice(actual: Double, expected: Double, product: String) {
    val delta = 0.01
    if (abs(actual / expected - 1) > delta) {
      // to propagate assertion error to UI, better than difference in true/false
      Assert.assertEquals(actual, expected, "Price $actual does not match $expected within $delta for $product product")
    }
  }
}
