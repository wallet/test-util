package com.budgetbakers.billing

import com.budgetbakers.be.billing.BraintreeClient
import com.budgetbakers.scripts.application.AbstractApplicationTest
import org.springframework.beans.factory.annotation.Autowired

abstract class AbstractBraintreeTest : AbstractApplicationTest() {

  @Autowired
  protected lateinit var braintreeClient: BraintreeClient
}
