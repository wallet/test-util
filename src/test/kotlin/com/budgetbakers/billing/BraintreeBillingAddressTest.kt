package com.budgetbakers.billing

import Flavor
import com.ribeez.RibeezBillingProtos.*
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test


class BraintreeBillingAddressTest : AbstractBraintreeTest() {

  private lateinit var products: List<BraintreeProduct>

  @BeforeClass
  override fun registerAndLogin() {
    registerAndLoginFlavor(Flavor.Board)
    initProducts()
  }

  private fun initProducts() {
    products = braintreeClient.getProducts(httpContext).recurrentProductsList
  }

  @Test(enabled = true)
  fun createSubscriptionWithBillingAddress() {
    createSubscription(products[0].product, "fake-valid-nonce", defaultBillingAddress())
  }

  @Test(enabled = true, dependsOnMethods = ["createSubscriptionWithBillingAddress"])
  fun createSubscriptionWithEmptyBillingAddress() {
    createSubscription(products[1].product, "fake-valid-no-billing-address-nonce", BraintreeBillingAddress.newBuilder().build())
  }

  @Test(enabled = true, dependsOnMethods = ["createSubscriptionWithEmptyBillingAddress"])
  fun createSubscriptionWithoutBillingAddress() {
    createSubscription(products[0].product, "fake-valid-visa-nonce")
  }

  @Test(enabled = true, dependsOnMethods = ["createSubscriptionWithoutBillingAddress"])
  fun updateBillingAddress() {
    val billingAddress = braintreeClient.getDetails(httpContext).billingAddress
    val updatedAddress = billingAddress.toBuilder()
      .setCompany("Company updated")
      .setCountryCodeAlpha2("US")
      .setStreetAddress("Street updated")
      .setExtendedAddress("Extended updated")
      .setCity("City updated")
      .setPostalCode("ZipUpdate")
      .setVatNumber("VAT updated")
      .setBusinessId("ID updated")
      .build()
    braintreeClient.updateBillingAddress(httpContext, updatedAddress)
  }

  protected fun createSubscription(product: Product, nonce: String, billingAddress: BraintreeBillingAddress? = null) {
    val transaction = Transaction.newBuilder()
      .setToken(nonce)
      .setProduct(product)
      .setTransactionId("")
      .build()

    if (billingAddress != null) {
      braintreeClient.updateBillingAddress(httpContext, billingAddress)
    }
    braintreeClient.createSubscription(httpContext, transaction)
  }

  protected fun defaultBillingAddress(): BraintreeBillingAddress {
    return BraintreeBillingAddress.newBuilder()
      .setCompany("company")
      .setCountryCodeAlpha2("CZ")
      .setStreetAddress("radlicka 180")
      .setExtendedAddress("naproti superchromu")
      .setCity("smichov")
      .setPostalCode("15000")
      .setVatNumber("CZ123456")
      .setBusinessId("123456")
      .build()
  }
}
