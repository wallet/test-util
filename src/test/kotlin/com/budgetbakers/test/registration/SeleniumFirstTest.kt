package com.budgetbakers.test.registration

import com.budgetbakers.test.registration.selenium.AbstractSeleniumTest
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.ExpectedConditions
import org.testng.annotations.Test

class SeleniumFirstTest : AbstractSeleniumTest() {

    @Test(enabled = false)
    fun first() {
        val profile = profileService.generate()

        driver.get(frontendUrl)
        val signInLink = By.cssSelector(".auth-content-header-link")
        wait.until(ExpectedConditions.visibilityOfElementLocated(signInLink)).click()
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys(profile.email)
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(profile.password)
        driver.findElement(By.xpath("//input[@name='passwordConfirmation']")).sendKeys(profile.password)
        driver.findElement(By.xpath("//input[@name='consent']/following-sibling::label")).click()
        driver.findElement(By.xpath("//input[@name='consent']/parent::div")).click()
        driver.findElement(By.xpath("//div[@class='signup-consent']/div/label")).click()
        driver.findElement(By.xpath("//div[@class='auth-button']/button")).click()
        // TODO not registered, must wait for a while
    }
}
