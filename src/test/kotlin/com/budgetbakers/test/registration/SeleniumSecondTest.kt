package com.budgetbakers.test.registration

import com.budgetbakers.test.registration.selenium.AbstractSeleniumTest
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.ExpectedConditions
import org.testng.annotations.Test

class SeleniumSecondTest : AbstractSeleniumTest() {

    @Test(enabled = false)
    fun second() {
        val profile = registrationService.register()
        driver.get(frontendUrl)

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='email']"))).sendKeys(profile.email)
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(profile.password)
        driver.findElement(By.xpath("//button[@type='submit']")).click()
        println("done")
    }
}
