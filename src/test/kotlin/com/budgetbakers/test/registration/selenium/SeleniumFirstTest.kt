package com.budgetbakers.test.registration.selenium

import com.budgetbakers.scripts.application.AbstractApplicationTest
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.WebDriverWait
import org.springframework.beans.factory.annotation.Value
import org.testng.annotations.AfterSuite
import org.testng.annotations.BeforeSuite

class AbstractSeleniumTest : AbstractApplicationTest() {

  @Value("\${frontend.url}")
  lateinit var frontendUrl: String

  protected lateinit var driver: WebDriver
  protected lateinit var wait: WebDriverWait

  @BeforeSuite
  fun setupTest() {
    driver = ChromeDriver()
    driver.manage().window().maximize()
    wait = WebDriverWait(driver, 15)
    log.info("suite prepared")
  }

  @AfterSuite
  fun teardown() {
    driver.quit()
    log.info("suite cleaned.")
  }

  protected fun executeScript(script: String, vararg args: Any) {
    val jsDriver = driver
    if (jsDriver is JavascriptExecutor) {
      jsDriver.executeScript(script, *args)
    }
  }

  protected fun getElementUnder(parent: By, child: By): WebElement {
    val parentEl = driver.findElement(parent)
    val childEl = parentEl.findElement(child)
    executeScript("""
                var elem = arguments[0];
                var text = elem.textContent;
                var parent = elem.parentNode;
                parent.removeChild(elem);
                parent.appendChild(document.createTextNode(text));
                """, childEl)
    return parentEl
  }
}
