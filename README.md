
edit src/main/resources/application.properties:
`server.port=8888`

and run: 

`gradle bootRun`

or run `gradle -PbootArgs=--server.port=8888 bootRun`, see build.gradle `bootRun` task how it process `bootArgs`

fetch data:

from adminer: `curl -u "xxxx:yyyy" "https://couch-prod-frankfurt-3.budgetbakers.com/bb-e6619bdf-48aa-4b3a-a1ff-372e99e5d93c/_changes?include_docs=true"`

change to

`curl -u "xxxx:yyyy" "https://couch-prod-frankfurt-3.budgetbakers.com/bb-e6619bdf-48aa-4b3a-a1ff-372e99e5d93c/_all_docs?include_docs=true" > data.json`

call clone endpoint:


`curl -H 'content-type: application/json' -X POST localhost:8888/clone/file?flavor=Board -d@data.json`
