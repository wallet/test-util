FROM openjdk:11.0.3-jre-slim-stretch
VOLUME /tmp
ARG DEPENDENCY=build/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app
# workaround for local development, can't be solved by .dockerignore because the Gradle plugin use different context
RUN find /app/ -name "*-local.properties" -exec rm {} \;
EXPOSE 8080
ENTRYPOINT ["java","-cp","app:app/lib/*","com.budgetbakers.scripts.application.ScriptsApplication"]
